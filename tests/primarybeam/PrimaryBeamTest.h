#include "primarybeam/PrimaryBeam.h"
#include "primarybeam/GaussianPB.h"
#include "primarybeam/PrimaryBeamFactory.h"
#include "linmos/LinmosAccumulator.h"
#include <casacore/casa/Arrays/Array.h>
#include <casacore/casa/Arrays/Cube.h>
#include <casacore/casa/Arrays/IPosition.h>

#include <casacore/coordinates/Coordinates/DirectionCoordinate.h>
#include <casacore/coordinates/Coordinates/SpectralCoordinate.h>
#include <casacore/coordinates/Coordinates/LinearCoordinate.h>
#include <casacore/coordinates/Coordinates/StokesCoordinate.h>

#include <casacore/coordinates/Coordinates/CoordinateSystem.h>
#include <casacore/fits/FITS/BasicFITS.h>

#include <askap/askap/AskapError.h>

#include <cppunit/extensions/HelperMacros.h>

#include <stdexcept>
#include <boost/shared_ptr.hpp>



using namespace casacore;


namespace askap
{
  namespace imagemath
  {

    class PrimaryBeamTest : public CppUnit::TestFixture
    {

      CPPUNIT_TEST_SUITE(PrimaryBeamTest);

      CPPUNIT_TEST(testCreateGaussian);
      CPPUNIT_TEST_EXCEPTION(testCreateAbstract,AskapError);
      CPPUNIT_TEST(testEvaluateGaussian);
      CPPUNIT_TEST(testCreate2dGaussian);
      CPPUNIT_TEST(testEvaluate2dGaussian);
      CPPUNIT_TEST(testCreateASKAPtaylor);
      CPPUNIT_TEST(testEvaluateASKAPtaylorA);
      CPPUNIT_TEST(testEvaluateASKAPtaylorB);
      CPPUNIT_TEST(testCreateASKAPtaylorV2);
      CPPUNIT_TEST(testEvaluateASKAPtaylorC);
      CPPUNIT_TEST(testEvaluateASKAPtaylorD);
      CPPUNIT_TEST(testCreateASKAPcube4d);
      CPPUNIT_TEST(testEvaluateASKAPcube4d);
      CPPUNIT_TEST(testCreateASKAPcube5d);
      CPPUNIT_TEST(testEvaluateASKAPcube5d);
      CPPUNIT_TEST(testExtractBeamNumberASKAP);
      CPPUNIT_TEST(testCreateMWA);
      CPPUNIT_TEST(testEvaluateMWA);
      CPPUNIT_TEST(testCreateSKALOW);
      CPPUNIT_TEST(testEvaluateSKALOW);
      CPPUNIT_TEST_SUITE_END();

  private:
      /// @brief method to access image for primary beam tests

      casacore::CoordinateSystem makeCoords() {

          // Direction Coordinate
          casacore::Matrix<casacore::Double> xform(2,2);                                    // 1
          xform = 0.0; xform.diagonal() = 1.0;                          // 2
          casacore::DirectionCoordinate radec(casacore::MDirection::J2000,                  // 3
              casacore::Projection(casacore::Projection::SIN),        // 4
              135*casacore::C::pi/180.0, 60*casacore::C::pi/180.0,    // 5
              -0.005*casacore::C::pi/180.0, 0.005*casacore::C::pi/180,        // 6
              xform,                              // 7
              128.0, 128.0,                       // 8
              999.0, 999.0);

          casacore::Vector<casacore::String> units(2); units = "deg";                        //  9
          radec.setWorldAxisUnits(units);                               // 10

          casacore::Vector<casacore::Double> world(2), pixel(2);                            // 11
          pixel = 138.0;                                                // 12

          CPPUNIT_ASSERT(radec.toWorld(world, pixel));                        // 13
                                                                  // 17
//          cout << world << " <--- " << pixel << endl;           // 18
          CPPUNIT_ASSERT(radec.toPixel(pixel, world));                             // 19

//          cout << world << " ---> " << pixel << endl;

          // StokesCoordinate
          Vector<Int> iquv(4);                                         // 20
          iquv(0) = Stokes::I; iquv(1) = Stokes::Q;                    // 21
          iquv(2) = Stokes::U; iquv(3) = Stokes::V;                    // 22
          StokesCoordinate stokes(iquv);                               // 23
          Int plane;                                                   // 24
          CPPUNIT_ASSERT(stokes.toPixel(plane, Stokes::Q));                       // 25

//          cout << "Stokes Q is plane " << plane << endl;

          CPPUNIT_ASSERT(stokes.toPixel(plane, Stokes::XX) == 0); // expecting to fail wrong Basis                      // 26



          // SpectralCoordinate
          SpectralCoordinate spectral(MFrequency::TOPO,               // 27
              1400 * 1.0E+6,                  // 28
              20 * 1.0E+3,                    // 29
              0,                              // 30
              1420.40575 * 1.0E+6);           // 31
          units.resize(1); pixel.resize(1); world.resize(1);
          units = "MHz";
          spectral.setWorldAxisUnits(units);

          pixel = 50;
          CPPUNIT_ASSERT(spectral.toWorld(world, pixel));

//          cout << world << " <--- " << pixel << endl;

          CPPUNIT_ASSERT(spectral.toPixel(pixel, world));

//          cout << world << " ---> " << pixel << endl;

          // CoordinateSystem
          CoordinateSystem coordsys;
          coordsys.addCoordinate(radec);
          // coordsys.addCoordinate(stokes);
          // coordsys.addCoordinate(spectral);


          return coordsys;
      }


  public:

      void setUp() {
          // create and write a constant into image

      }
      void tearDown() {
      }
      void testEvaluateGaussian() {
          LOFAR::ParameterSet parset;

          const casacore::IPosition shape(2,256,256);
          casacore::Array<float> arr(shape);
          arr.set(1.);
          casacore::CoordinateSystem coordsys(makeCoords());


          casacore::Vector<double> pixel(2,0.);
          casacore::MVDirection world;
          double offsetBeam = 0;
          double frequency = 1.0E9; // 1GHz


          parset.add("aperture", "12");
          parset.add("fwhmscaling", "0.5");
          parset.add("primarybeam","GaussianPB");
          PrimaryBeam::ShPtr GaussPB =  PrimaryBeamFactory::make(parset);


          // reference direction of input coords
          casacore::MVDirection beamcentre(135*C::pi/180.0, 60*C::pi/180.0);


          // get coordinates of the direction axes
          const int dcPos = coordsys.findCoordinate(casacore::Coordinate::DIRECTION,-1);
          const casacore::DirectionCoordinate outDC = coordsys.directionCoordinate(dcPos);

          casacore::Array<float> BeamArr(shape);

          for (int x=0; x<shape[0];++x) {
              for (int y=0; y<shape[1];++y) {

                  // get the current pixel location and distance from beam centre
                  pixel[0] = double(x);
                  pixel[1] = double(y);
                  //cout << " pixel --- " << pixel << endl;
                  outDC.toWorld(world,pixel);

  //                cout << " world --- " << world << endl;
    //              cout << " beamcentres -- " << beamcentre << endl;

                  offsetBeam = beamcentre.separation(world);
      //            cout << " offset -- " << offsetBeam << endl;

                  double beamval = GaussPB->evaluateAtOffset(offsetBeam,frequency);
        //          cout << " beamval --- " << beamval << endl;

                  const casacore::IPosition index(2,int(x),int(y));
                  BeamArr(index) = beamval*beamval;


              }
          }
          const casacore::IPosition index(2,200,200);
          double testVal = BeamArr(index);
          // 0.49583 is the val at 200,200
          pixel[0] = 200;
          pixel[1] = 200;
          outDC.toWorld(world,pixel);

          offsetBeam = beamcentre.separation(world);
          double beamval = GaussPB->evaluateAtOffset(offsetBeam,frequency);
          // cout << "testVal: " << testVal << " beamVal: " << beamval*beamval << endl;

          CPPUNIT_ASSERT(abs(testVal - beamval*beamval) < 1E-7);


      }
      void testCreateGaussian()
      {

         LOFAR::ParameterSet parset;
         parset.add("aperture", "12");
         parset.add("fwhmscaling", "0.5");
         parset.add("primarybeam","GaussianPB");
         PrimaryBeam::ShPtr PB = PrimaryBeamFactory::make(parset);

      }
      void testCreate2dGaussian()
      {

         LOFAR::ParameterSet parset;
         parset.add("xwidth", "0.002");
         parset.add("ywidth", "0.002");
         parset.add("primarybeam","GaussianPB");
         PrimaryBeam::ShPtr PB = PrimaryBeamFactory::make(parset);

      }
      void testEvaluate2dGaussian() {
          LOFAR::ParameterSet parset;

          const casacore::IPosition shape(2,256,256);
          casacore::Array<float> arr(shape);
          arr.set(1.);
          casacore::CoordinateSystem coordsys(makeCoords());


          casacore::Vector<double> pixel(2,0.);
          casacore::MVDirection world;
          double offsetBeam = 0;
          double frequency = 1.0E9; // 1GHz

          parset.add("primarybeam","GaussianPB");
          parset.add("primarybeam.GaussianPB.xwidth", "0.002");
          parset.add("primarybeam.GaussianPB.ywidth", "0.002");



          PrimaryBeam::ShPtr GaussPB =  PrimaryBeamFactory::make(parset);


          // reference direction of input coords
          casacore::MVDirection beamcentre(135*C::pi/180.0, 60*C::pi/180.0);


          // get coordinates of the direction axes
          const int dcPos = coordsys.findCoordinate(casacore::Coordinate::DIRECTION,-1);
          const casacore::DirectionCoordinate outDC = coordsys.directionCoordinate(dcPos);

          casacore::Array<float> BeamArr(shape);

          for (int x=0; x<shape[0];++x) {
              for (int y=0; y<shape[1];++y) {

                  // get the current pixel location and distance from beam centre
                  pixel[0] = double(x);
                  pixel[1] = double(y);
                  //cout << " pixel --- " << pixel << endl;
                  outDC.toWorld(world,pixel);

             //     cout << " world --- " << world << endl;
               //   cout << " beamcentres -- " << beamcentre << endl;

                  offsetBeam = beamcentre.separation(world);
              //    cout << " offset -- " << offsetBeam << endl;

                  double beamval = GaussPB->evaluateAtOffset(offsetBeam,frequency);
              //    cout << " beamval --- " << beamval << endl;

                  const casacore::IPosition index(2,int(x),int(y));
                  BeamArr(index) = beamval*beamval;


              }
          }
          const casacore::IPosition index(2,200,200);
          double testVal = BeamArr(index);
          // 0.49583 is the val at 200,200
          pixel[0] = 200;
          pixel[1] = 200;
          outDC.toWorld(world,pixel);

          offsetBeam = beamcentre.separation(world);
          double beamval = GaussPB->evaluateAtOffset(offsetBeam,frequency);
          // cout << "2D-testVal: " << testVal << " 2D-beamVal: " << beamval*beamval << endl;

          CPPUNIT_ASSERT(abs(testVal - beamval*beamval) < 1E-7);


      }

      void testCreateASKAPtaylor()
      {
         // Create test beam image - fill it with a gaussian on a sparse grid

         const String FileName("../../Testing/Temporary/askap-gauss-taylor.fits");
         const int nx = 31;
         const int ny = 31;
         const int ntaylor = 3;
         const int nbeam = 1;
         casacore::Matrix<float> mat(nx,ny);
         casacore::Array<float> arr(casacore::IPosition(4,nx,ny,ntaylor,nbeam));
         String ErrorMessage;
         const String unit("Jy/beam");
         casacore::Vector<casacore::String> axisNames(4);
         axisNames(0) = "RA---SIN";
         axisNames(1) = "DEC--SIN";
         axisNames(2) = "FREQ";
         axisNames(3) = "BEAM";
         casacore::Vector<float> refPix(4,0.0);
         refPix(0) = nx/2;
         refPix(1) = ny/2;
         casacore::Vector<float> refVal(4,0.0);
         refVal(2) = 850.e6;
         casacore::Vector<float> delta(4,1.0);
         delta(0) = -0.1;
         delta(1) = 0.1;
         std::map< String, Double > keywords;
         keywords.insert(std::make_pair("EQUINOX",2000.0));

         // create GaussianPB to fill the array
         LOFAR::ParameterSet parset;
         parset.add("aperture", "12");
         parset.add("fwhmscaling", "1.09");
         parset.add("primarybeam","GaussianPB");
         PrimaryBeam::ShPtr GaussianPB = PrimaryBeamFactory::make(parset);

         float alpha=0;
         float beta=0;
         IPosition pix(4);
         pix(3) = 0;
         for (int y=0; y<ny; y++) {
             pix(1) = y;
             for (int x=0; x<nx; x++) {
                 pix(0) = x;
                 double offx = double(x-refPix(0)) * delta(0)*casacore::C::degree;
                 double offy = double(y-refPix(1)) * delta(1)*casacore::C::degree;

                 double dist = asin( sqrt( offx*offx + offy*offy ) );
                 double pa = atan2(offx, offy);

                 double pb = GaussianPB->evaluateAtOffset(pa,dist,refVal(2));
                 GaussianPB->evaluateAlphaBetaAtOffset(alpha, beta, pa, dist, refVal(2));
                 pix(2) = 0;
                 arr(pix) = pb;
                 pix(2) = 1;
                 arr(pix) = alpha * pb;
                 pix(2) = 2;
                 arr(pix) = pb * (beta + alpha * (alpha-1)/2);
             }
         }

         casacore::WriteFITS(FileName.chars(), arr, ErrorMessage, unit.chars(),
            &axisNames, &refPix, &refVal, &delta, &keywords);

         // now create ASKAP_PB using this fits file
         LOFAR::ParameterSet parset2;
         parset2.add("primarybeam","ASKAP_PB");
         parset2.add("primarybeam.ASKAP_PB.image",FileName);
         parset2.add("primarybeam.ASKAP_PB.nterms","3");
         PrimaryBeam::ShPtr PB = PrimaryBeamFactory::make(parset2);
      }

      void testCreateASKAPtaylorV2()
      {
         // Create test beam image - fill it with a gaussian on a sparse grid

         const String FileName("../../Testing/Temporary/askap-gauss-taylorV2.fits");
         const int nx = 31;
         const int ny = 31;
         const int nfreq = 1;
         const int ntaylor = 3;
         const int nstokes = 4; // if set to 1, casacore removes axis on read
         const int nbeam = 1;
         casacore::Matrix<float> mat(nx,ny);
         casacore::Array<float> arr(casacore::IPosition(6,nx,ny,nfreq,ntaylor,nstokes,nbeam));
         String ErrorMessage;
         const String unit("Jy/beam");
         casacore::Vector<casacore::String> axisNames(6);
         axisNames(0) = "RA---SIN";
         axisNames(1) = "DEC--SIN";
         axisNames(2) = "FREQ";
         axisNames(3) = "TAYLOR";
         axisNames(4) = "STOKES";
         axisNames(5) = "BEAM";
         casacore::Vector<float> refPix(6,0.0);
         refPix(0) = nx/2;
         refPix(1) = ny/2;
         casacore::Vector<float> refVal(6,0.0);
         refVal(2) = 850.e6;
         refVal(4) = 1.0;
         casacore::Vector<float> delta(6,1.0);
         delta(0) = -0.1;
         delta(1) = 0.1;
         std::map< String, Double > keywords;
         keywords.insert(std::make_pair("EQUINOX",2000.0));

         // create GaussianPB to fill the array
         LOFAR::ParameterSet parset;
         parset.add("aperture", "12");
         parset.add("fwhmscaling", "1.09");
         parset.add("primarybeam","GaussianPB");
         PrimaryBeam::ShPtr GaussianPB = PrimaryBeamFactory::make(parset);

         float alpha=0;
         float beta=0;
         IPosition pix(6,0);
         for (int y=0; y<ny; y++) {
             pix(1) = y;
             for (int x=0; x<nx; x++) {
                 pix(0) = x;
                 double offx = double(x-refPix(0)) * delta(0)*casacore::C::degree;
                 double offy = double(y-refPix(1)) * delta(1)*casacore::C::degree;

                 double dist = asin( sqrt( offx*offx + offy*offy ) );
                 double pa = atan2(offx, offy);

                 double pb = GaussianPB->evaluateAtOffset(pa,dist,refVal(2));
                 GaussianPB->evaluateAlphaBetaAtOffset(alpha, beta, pa, dist, refVal(2));
                 pix(3) = 0;
                 arr(pix) = pb;
                 pix(3) = 1;
                 arr(pix) = alpha * pb;
                 pix(3) = 2;
                 arr(pix) = pb * (beta + alpha * (alpha-1)/2);
             }
         }

         casacore::WriteFITS(FileName.chars(), arr, ErrorMessage, unit.chars(),
            &axisNames, &refPix, &refVal, &delta, &keywords);

         // now create ASKAP_PB using this fits file
         LOFAR::ParameterSet parset2;
         parset2.add("primarybeam","ASKAP_PB");
         parset2.add("primarybeam.ASKAP_PB.image",FileName);
         parset2.add("primarybeam.ASKAP_PB.nterms","3");
         PrimaryBeam::ShPtr PB = PrimaryBeamFactory::make(parset2);
      }

      void testEvaluateASKAPtaylorA() {
          // Test at reference freq of beam model
          testEvaluateASKAPtaylor(850.0E6,0.01);
      }

      void testEvaluateASKAPtaylorB() {
          // Test at different freq from beam model reference
          // need to increase error tolerance for alpha, beta
          testEvaluateASKAPtaylor(880.0E6,0.1);
      }

      void testEvaluateASKAPtaylorC() {
          // Test at reference freq of beam model
          testEvaluateASKAPtaylorV2(850.0E6,0.01);
      }

      void testEvaluateASKAPtaylorD() {
          // Test at different freq from beam model reference
          // need to increase error tolerance for alpha, beta
          testEvaluateASKAPtaylorV2(880.0E6,0.1);
      }

      void testEvaluateASKAPtaylor(double frequency, double tol) {
          LOFAR::ParameterSet parset;

          cout << "ASKAP_PB taylor test" << endl;

          const casacore::IPosition shape(2,256,256);
          casacore::Array<float> arr(shape);
          arr.set(1.);
          casacore::CoordinateSystem coordsys(makeCoords());

          casacore::IPosition pixel(2,0.);
          casacore::Vector<double> offset(2,0.);
          double res = 0.5 / 60. * C::pi/180.; // 0.5'
          double dist = 0;
          double pa = 0;
          //double frequency = 850.0E6; // 850MHz

          parset.add("primarybeam","ASKAP_PB");
          parset.add("primarybeam.ASKAP_PB.image","../../Testing/Temporary/askap-gauss-taylor.fits");
          parset.add("primarybeam.ASKAP_PB.nterms","3");
          PrimaryBeam::ShPtr PB =  PrimaryBeamFactory::make(parset);

          // get coordinates of the direction axes
          const int dcPos = coordsys.findCoordinate(casacore::Coordinate::DIRECTION,-1);
          const casacore::DirectionCoordinate outDC = coordsys.directionCoordinate(dcPos);

          casacore::Matrix<double> BeamArr(shape);

          for (int x=0; x<shape[0];++x) {
              for (int y=0; y<shape[1];++y) {

                  // get the current pixel location and distance from beam centre
                  pixel[0] = x;
                  pixel[1] = y;
                  offset[0] = double(pixel[0]-shape[0]/2) * res;
                  offset[1] = double(pixel[1]-shape[1]/2) * res;

                  dist = asin( sqrt( offset[0]*offset[0] + offset[1]*offset[1] ) );
                  pa = atan2( offset[0], offset[1] );

                  double pb = PB->evaluateAtOffset(pa,dist,frequency);

                  BeamArr(x,y) = pb;

              }
          }
          pixel[0] = 201;
          pixel[1] = 201;
          double testVal = BeamArr(pixel);
          offset[0] = double(pixel[0]-shape[0]/2) * res;
          offset[1] = double(pixel[1]-shape[1]/2) * res;
          dist = asin( sqrt( offset[0]*offset[0] + offset[1]*offset[1] ) );
          pa = atan2( offset[0], offset[1] );

          double pb = PB->evaluateAtOffset(pa,dist,frequency);
          double beamVal = pb;
          float beamAlpha = 0;
          float beamBeta = 0;
          PB->evaluateAlphaBetaAtOffset(beamAlpha, beamBeta, pa,dist,frequency);

          double gausVal = 0;
          float gausAlpha = 0;
          float gausBeta = 0;
          {
              // create GaussianPB to compare with interpolated value
              LOFAR::ParameterSet parset;
              parset.add("aperture", "12");
              parset.add("fwhmscaling", "1.09");
              parset.add("primarybeam","GaussianPB");
              PrimaryBeam::ShPtr GaussianPB = PrimaryBeamFactory::make(parset);
              gausVal = GaussianPB->evaluateAtOffset(pa,dist,frequency);
              GaussianPB->evaluateAlphaBetaAtOffset(gausAlpha,gausBeta,pa,dist,frequency);
          }
          cout << "testVal: " << testVal << " beamVal: " << beamVal << " gausVal: " << gausVal << endl;
          cout << "BeamAlpha: " << beamAlpha << " gausAlpha: " << gausAlpha << " beamBeta: " << beamBeta <<
           " gausBeta: " << gausBeta << endl;

          CPPUNIT_ASSERT(abs(testVal - beamVal) < 1E-7);
          CPPUNIT_ASSERT(abs(testVal - gausVal) < 1E-3);
          CPPUNIT_ASSERT(abs(beamAlpha - gausAlpha) < tol);
          CPPUNIT_ASSERT(abs(beamBeta - gausBeta) < tol);

          cout << "ASKAP_PB taylor test complete" << endl;

      }

      void testEvaluateASKAPtaylorV2(double frequency, double tol) {
          LOFAR::ParameterSet parset;

          cout << "ASKAP_PB taylor V2 test" << endl;

          const casacore::IPosition shape(2,256,256);
          casacore::Array<float> arr(shape);
          arr.set(1.);
          casacore::CoordinateSystem coordsys(makeCoords());

          casacore::IPosition pixel(2,0.);
          casacore::Vector<double> offset(2,0.);
          double res = 0.5 / 60. * C::pi/180.; // 0.5'
          double dist = 0;
          double pa = 0;
          //double frequency = 850.0E6; // 850MHz

          parset.add("primarybeam","ASKAP_PB");
          parset.add("primarybeam.ASKAP_PB.image","../../Testing/Temporary/askap-gauss-taylorV2.fits");
          parset.add("primarybeam.ASKAP_PB.nterms","3");
          PrimaryBeam::ShPtr PB =  PrimaryBeamFactory::make(parset);

          // get coordinates of the direction axes
          const int dcPos = coordsys.findCoordinate(casacore::Coordinate::DIRECTION,-1);
          const casacore::DirectionCoordinate outDC = coordsys.directionCoordinate(dcPos);

          casacore::Matrix<double> BeamArr(shape);

          for (int x=0; x<shape[0];++x) {
              for (int y=0; y<shape[1];++y) {

                  // get the current pixel location and distance from beam centre
                  pixel[0] = x;
                  pixel[1] = y;
                  offset[0] = double(pixel[0]-shape[0]/2) * res;
                  offset[1] = double(pixel[1]-shape[1]/2) * res;

                  dist = asin( sqrt( offset[0]*offset[0] + offset[1]*offset[1] ) );
                  pa = atan2( offset[0], offset[1] );

                  double pb = PB->evaluateAtOffset(pa,dist,frequency);

                  BeamArr(x,y) = pb;

              }
          }
          pixel[0] = 201;
          pixel[1] = 201;
          double testVal = BeamArr(pixel);
          offset[0] = double(pixel[0]-shape[0]/2) * res;
          offset[1] = double(pixel[1]-shape[1]/2) * res;
          dist = asin( sqrt( offset[0]*offset[0] + offset[1]*offset[1] ) );
          pa = atan2( offset[0], offset[1] );

          double pb = PB->evaluateAtOffset(pa,dist,frequency);
          double beamVal = pb;
          float beamAlpha = 0;
          float beamBeta = 0;
          PB->evaluateAlphaBetaAtOffset(beamAlpha, beamBeta, pa,dist,frequency);

          double gausVal = 0;
          float gausAlpha = 0;
          float gausBeta = 0;
          {
              // create GaussianPB to compare with interpolated value
              LOFAR::ParameterSet parset;
              parset.add("aperture", "12");
              parset.add("fwhmscaling", "1.09");
              parset.add("primarybeam","GaussianPB");
              PrimaryBeam::ShPtr GaussianPB = PrimaryBeamFactory::make(parset);
              gausVal = GaussianPB->evaluateAtOffset(pa,dist,frequency);
              GaussianPB->evaluateAlphaBetaAtOffset(gausAlpha,gausBeta,pa,dist,frequency);
          }
          cout << "testVal: " << testVal << " beamVal: " << beamVal << " gausVal: " << gausVal << endl;
          cout << "BeamAlpha: " << beamAlpha << " gausAlpha: " << gausAlpha << " beamBeta: " << beamBeta <<
           " gausBeta: " << gausBeta << endl;

          CPPUNIT_ASSERT(abs(testVal - beamVal) < 1E-7);
          CPPUNIT_ASSERT(abs(testVal - gausVal) < 1E-3);
          CPPUNIT_ASSERT(abs(beamAlpha - gausAlpha) < tol);
          CPPUNIT_ASSERT(abs(beamBeta - gausBeta) < tol);

          cout << "ASKAP_PB taylor V2 test complete" << endl;

      }

      void createASKAPcube(bool StokesAxis = true)
      {
          // Create test beam image - fill it with a gaussian on a sparse grid

          String FileName;
          if (StokesAxis) {
              FileName = "../../Testing/Temporary/askap-gauss-cube5d.fits";
          } else {
              FileName = "../../Testing/Temporary/askap-gauss-cube4d.fits";
          }
          const int nx = 31;
          const int ny = 31;
          const int nchan = 3;
          const int npol = 4;
          const int nbeam = 1;
          casacore::Matrix<float> mat(nx,ny);
          casacore::IPosition shape;
          int ndim = 5;
          if (StokesAxis) {
              shape = IPosition(5,nx,ny,nchan,npol,nbeam);
          } else {
              shape = IPosition(4,nx,ny,nchan,nbeam);
              ndim = 4;
          }
          casacore::Array<float> arr(shape);
          String ErrorMessage;
          const String unit("Jy/beam");
          casacore::Vector<casacore::String> axisNames(ndim);
          axisNames(0) = "RA---SIN";
          axisNames(1) = "DEC--SIN";
          axisNames(2) = "FREQ";
          if (StokesAxis) {
              axisNames(3) = "STOKES";
          }
          axisNames(ndim-1) = "BEAM";
          casacore::Vector<float> refPix(ndim,0.0);
          refPix(0) = nx/2;
          refPix(1) = ny/2;
          refPix(2) = 1;
          casacore::Vector<float> refVal(ndim,0.0);
          refVal(2) = 850.e6;
          refVal(3) = 1;
          refVal(ndim-1) = 0;
          casacore::Vector<float> delta(ndim,1.0);
          delta(0) = -0.1;
          delta(1) = 0.1;
          delta(2) = 10.e6;
          std::map< String, Double > keywords;
          keywords.insert(std::make_pair("EQUINOX",2000.0));

          // create GaussianPB to fill the array
          LOFAR::ParameterSet parset;
          parset.add("aperture", "12");
          parset.add("fwhmscaling", "1.09");
          parset.add("primarybeam","GaussianPB");
          PrimaryBeam::ShPtr GaussianPB = PrimaryBeamFactory::make(parset);

          float alpha=0;
          float beta=0;
          IPosition pix(ndim,0);
          for (int y=0; y<ny; y++) {
              pix(1) = y;
              for (int x=0; x<nx; x++) {
                  pix(0) = x;
                  double offx = double(x-refPix(0)) * delta(0)*casacore::C::degree;
                  double offy = double(y-refPix(1)) * delta(1)*casacore::C::degree;

                  double dist = asin( sqrt( offx*offx + offy*offy ) );
                  double pa = atan2(offx, offy);

                  for (int z=0; z<nchan; z++) {
                      double freq = refVal(2) + (z-refPix(2)) * delta(2);
                      double pb = GaussianPB->evaluateAtOffset(pa,dist,freq);
                      pix(2) = z;
                      pix(3) = 0;
                      arr(pix) = pb; // Stokes I
                      if (StokesAxis) {
                          pix(3) = 1;
                          arr(pix) = pb * sin(pa) * sin(dist); // Q
                          pix(3) = 2;
                          arr(pix) = -pb * cos(pa) * sin(dist); // U
                          pix(3) = 3;
                          arr(pix) = 0.1 * pb * sin(dist); // V
                      }
                  }
              }
          }

          casacore::WriteFITS(FileName.chars(), arr, ErrorMessage, unit.chars(),
             &axisNames, &refPix, &refVal, &delta, &keywords);

          // now create ASKAP_PB using this fits file
          LOFAR::ParameterSet parset2;
          parset2.add("primarybeam","ASKAP_PB");
          parset2.add("primarybeam.ASKAP_PB.image",FileName);
          parset2.add("primarybeam.ASKAP_PB.nterms","0");
          PrimaryBeam::ShPtr PB = PrimaryBeamFactory::make(parset2);

      }

      void testCreateASKAPcube4d() {
          createASKAPcube(false);
      }

      void testCreateASKAPcube5d() {
          createASKAPcube(true);
      }

      void testEvaluateASKAPcube4d() {
          testEvaluateASKAPcube(false);
      }

      void testEvaluateASKAPcube5d() {
          testEvaluateASKAPcube(true);
      }
      void testEvaluateASKAPcube(bool StokesAxis)
      {
          LOFAR::ParameterSet parset;

          cout << "ASKAP_PB cube test" << endl;

          const casacore::IPosition shape(2,256,256);
          casacore::Array<float> arr(shape);
          arr.set(1.);
          casacore::CoordinateSystem coordsys(makeCoords());

          casacore::IPosition pixel(2,0.);
          casacore::Vector<double> offset(2,0.);
          double res = 0.5 / 60. * C::pi/180.; // 0.5'
          double dist = 0;
          double pa = 0;
          double frequency = 850.0E6; // 850MHz

          parset.add("primarybeam","ASKAP_PB");
          if (StokesAxis) {
              parset.add("primarybeam.ASKAP_PB.image","../../Testing/Temporary/askap-gauss-cube5d.fits");
          } else {
              parset.add("primarybeam.ASKAP_PB.image","../../Testing/Temporary/askap-gauss-cube4d.fits");
          }
          parset.add("primarybeam.ASKAP_PB.nterms","0");
          PrimaryBeam::ShPtr PB =  PrimaryBeamFactory::make(parset);

          // get coordinates of the direction axes
          const int dcPos = coordsys.findCoordinate(casacore::Coordinate::DIRECTION,-1);
          const casacore::DirectionCoordinate outDC = coordsys.directionCoordinate(dcPos);

          casacore::Matrix<double> BeamArr(shape);
          casacore::IPosition lshape(3,shape(0),shape(1),3);
          casacore::Cube<double> LeakageArr(lshape);

          for (int x=0; x<shape[0];++x) {
              for (int y=0; y<shape[1];++y) {

                  // get the current pixel location and distance from beam centre
                  pixel[0] = x;
                  pixel[1] = y;
                  offset[0] = double(pixel[0]-shape[0]/2) * res;
                  offset[1] = double(pixel[1]-shape[1]/2) * res;

                  dist = asin( sqrt( offset[0]*offset[0] + offset[1]*offset[1] ) );
                  pa = atan2( offset[0], offset[1] );

                  double pb = PB->evaluateAtOffset(pa,dist,frequency);

                  BeamArr(x,y) = pb;

                  if (StokesAxis) {
                      double pbQ = PB->getLeakageAtOffset(pa,dist,1,frequency);
                      double pbU = PB->getLeakageAtOffset(pa,dist,2,frequency);
                      double pbV = PB->getLeakageAtOffset(pa,dist,3,frequency);
                      LeakageArr(x,y,0) = pbQ;
                      LeakageArr(x,y,1) = pbU;
                      LeakageArr(x,y,2) = pbV;
                  }

              }
          }
          pixel[0] = 201;
          pixel[1] = 201;
          double testVal = BeamArr(pixel);
          offset[0] = double(pixel[0]-shape[0]/2) * res;
          offset[1] = double(pixel[1]-shape[1]/2) * res;
          dist = asin( sqrt( offset[0]*offset[0] + offset[1]*offset[1] ) );
          pa = atan2( offset[0], offset[1] );

          double pb = PB->evaluateAtOffset(pa,dist,frequency);
          double beamVal = pb;
          float beamAlpha = 0;
          float beamBeta = 0;
          PB->evaluateAlphaBetaAtOffset(beamAlpha, beamBeta, pa,dist,frequency);

          double gausVal = 0;
          float gausAlpha = 0;
          float gausBeta = 0;
          {
              // create GaussianPB to compare with interpolated value
              LOFAR::ParameterSet parset;
              parset.add("aperture", "12");
              parset.add("fwhmscaling", "1.09");
              parset.add("primarybeam","GaussianPB");
              PrimaryBeam::ShPtr GaussianPB = PrimaryBeamFactory::make(parset);
              gausVal = GaussianPB->evaluateAtOffset(pa,dist,frequency);
              GaussianPB->evaluateAlphaBetaAtOffset(gausAlpha,gausBeta,pa,dist,frequency);
          }
          cout << "testVal: " << testVal << " beamVal: " << beamVal << " gausVal: " << gausVal << endl;
          cout << "BeamAlpha: " << beamAlpha << " gausAlpha: " << gausAlpha << " beamBeta: " << beamBeta <<
           " gausBeta: " << gausBeta << endl;

          CPPUNIT_ASSERT(abs(testVal - beamVal) < 1E-7);
          CPPUNIT_ASSERT(abs(testVal - gausVal) < 1E-3);
          CPPUNIT_ASSERT(abs(beamAlpha - gausAlpha) < 1E-2);
          CPPUNIT_ASSERT(abs(beamBeta - gausBeta) < 1E-2);

          if (StokesAxis) {
              // Test Q, U, V leakage values
              double pbQ = PB->getLeakageAtOffset(pa,dist,1,frequency);
              double pbU = PB->getLeakageAtOffset(pa,dist,2,frequency);
              double pbV = PB->getLeakageAtOffset(pa,dist,3,frequency);
              double gpbQ = gausVal * sin(pa) * sin(dist); // Q
              double gpbU = -gausVal * cos(pa) * sin(dist); // U
              double gpbV = gausVal * 0.1 * sin(dist); // V
              casacore::IPosition polPixel(3,pixel[0],pixel[1],0);
              double testValQ = LeakageArr(polPixel);
              polPixel(2) = 1;
              double testValU = LeakageArr(polPixel);
              polPixel(2) = 2;
              double testValV = LeakageArr(polPixel);
              cout << "Q: testVal: " << testValQ << " beamVal: " << pbQ << " gausVal: " << gpbQ << endl;
              cout << "U: testVal: " << testValU << " beamVal: " << pbU << " gausVal: " << gpbU << endl;
              cout << "V: testVal: " << testValV << " beamVal: " << pbV << " gausVal: " << gpbV << endl;

              CPPUNIT_ASSERT(abs(testValQ - pbQ) < 1E-7);
              CPPUNIT_ASSERT(abs(testValQ - gpbQ) < 1E-4);
              CPPUNIT_ASSERT(abs(testValU - pbU) < 1E-7);
              CPPUNIT_ASSERT(abs(testValU - gpbU) < 1E-4);
              CPPUNIT_ASSERT(abs(testValV - pbV) < 1E-7);
              CPPUNIT_ASSERT(abs(testValV - gpbV) < 1E-5);
          }


          cout << "ASKAP_PB cube test complete" << endl;

      }

      void testExtractBeamNumberASKAP()
      {
          LinmosAccumulator<float> acc;

          // test beamxx in middle of name
          {
              cout << "LinmosAccumulator::getBeamFromImageName test"<<endl;
              LOFAR::ParameterSet myparset;
              myparset.replace("names","[image.restored.u.SB10007.contcube.POSSUM_2126-54.beam00.conv,image.restored.u.SB10007.contcube.POSSUM_2126-54.beam35.conv]");
              vector<string> names = myparset.getStringVector("names",true);
              vector<int> beams = { 0, 35};
              for (size_t i = 0; i < beams.size(); i++) {
                  cout <<"Reading beam number from "<< names[i] <<" : "<< acc.getBeamFromImageName(names[i])<<endl;
                  CPPUNIT_ASSERT(beams[i] == acc.getBeamFromImageName(names[i]));
              }
          }

          // test beamxx at end of name
          {
              LOFAR::ParameterSet myparset;
              myparset.replace("names","[image.restored.u.SB10007.contcube.POSSUM_2126-54.beam00,image.restored.u.SB10007.contcube.POSSUM_2126-54.beam35]");
              vector<string> names = myparset.getStringVector("names",true);
              vector<int> beams = { 0, 35};
              for (size_t i = 0; i < beams.size(); i++) {
                  cout <<"Reading beam number from "<< names[i] <<" : "<< acc.getBeamFromImageName(names[i])<<endl;
                  CPPUNIT_ASSERT(beams[i] == acc.getBeamFromImageName(names[i]));
              }
          }

          // test beamxx in range
          {
              LOFAR::ParameterSet myparset;
              myparset.replace("names","[image.restored.u.SB10007.contcube.POSSUM_2126-54.beam11..13]");
              vector<string> names = myparset.getStringVector("names",true);
              vector<int> beams = { 11, 12, 13};
              for (size_t i = 0; i < beams.size(); i++) {
                  cout <<"Reading beam number from "<< names[i] <<" : "<< acc.getBeamFromImageName(names[i])<<endl;
                  CPPUNIT_ASSERT(beams[i] == acc.getBeamFromImageName(names[i]));
              }
          }

          cout << "LinmosAccumulator::getBeamFromImageName test complete"<<endl;
      }


      void testCreateMWA()
      {
         LOFAR::ParameterSet parset;
         parset.add("primarybeam","MWA_PB");
         PrimaryBeam::ShPtr PB = PrimaryBeamFactory::make(parset);
      }
      void testEvaluateMWA() {
          LOFAR::ParameterSet parset;

          // cout << "MWA_PB test" << endl;

          const casacore::IPosition shape(2,256,256);
          casacore::Array<float> arr(shape);
          arr.set(1.);
          casacore::CoordinateSystem coordsys(makeCoords());

          casacore::IPosition pixel(2,0.);
          casacore::Vector<double> offset(2,0.);
          double res = 1. / 60. * C::pi/180.; // 1'
          double az = 0;
          double za = 0;
          double frequency = 150.0E6; // 150MHz

          parset.add("primarybeam","MWA_PB");
          PrimaryBeam::ShPtr PB =  PrimaryBeamFactory::make(parset);

          // get coordinates of the direction axes
          const int dcPos = coordsys.findCoordinate(casacore::Coordinate::DIRECTION,-1);
          const casacore::DirectionCoordinate outDC = coordsys.directionCoordinate(dcPos);

          casacore::Array<double> BeamArr(shape);

          for (int x=0; x<shape[0];++x) {
              for (int y=0; y<shape[1];++y) {

                  // get the current pixel location and distance from beam centre
                  pixel[0] = x;
                  pixel[1] = y;
                  offset[0] = double(pixel[0]-shape[0]/2) * res;
                  offset[1] = double(pixel[1]-shape[1]/2) * res;

                  za = asin( sqrt( offset[0]*offset[0] + offset[1]*offset[1] ) );
                  az = atan2( offset[0], offset[1] );

                  casacore::Matrix<casacore::Complex> J = PB->getJones(az,za,frequency);

                  //cout << " offset --- " << offset << " az,za = " << az << ", " << za << ", J = " << J << endl;

                  BeamArr(pixel) = real( J(0,0)*conj(J(0,0)) + J(0,1)*conj(J(0,1)) );

              }
          }
          pixel[0] = 200;
          pixel[1] = 200;
          double testVal = BeamArr(pixel);
          // 0.49583 is the val at 200,200
          offset[0] = double(pixel[0]-shape[0]/2) * res;
          offset[1] = double(pixel[1]-shape[1]/2) * res;
          za = asin( sqrt( offset[0]*offset[0] + offset[1]*offset[1] ) );
          az = atan2( offset[0], offset[1] );

          casacore::Matrix<casacore::Complex> J = PB->getJones(az,za,frequency);

          double beamVal = real( J(0,0)*conj(J(0,0)) + J(0,1)*conj(J(0,1)) );

          // cout << "testVal: " << testVal << " beamVal: " << beamVal << endl;

          CPPUNIT_ASSERT(abs(testVal - beamVal) < 1E-7);

          cout << "MWA_PB test complete" << endl;

      }
      void testCreateSKALOW()
      {
         LOFAR::ParameterSet parset;
         parset.add("primarybeam","SKA_LOW_PB");
         PrimaryBeam::ShPtr PB = PrimaryBeamFactory::make(parset);
      }
      void testEvaluateSKALOW() {
          LOFAR::ParameterSet parset;
          cout << "SKA_LOW_PB test" << endl;

          const casacore::IPosition shape(2,256,256);
          //const casacore::IPosition shape(2,256,256);
          casacore::Array<float> arr(shape);
          arr.set(1.);
          casacore::CoordinateSystem coordsys(makeCoords());

          casacore::IPosition pixel(2,0.);
          double res = 1. / 60. * C::pi/180.; // 1'
          double l, m, n;
          double frequency = 150.0E6; // 150MHz
          double ra = 0;
          double dec = 0;
          double lat = -26.703319 * casacore::C::degree; // MWA latitude
          double lst = 0.0; // set some local time to convert between HA and RA

          const int testx = 200;
          const int testy = 150;
          casacore::IPosition testpixel(2,0.);
          testpixel[0] = testx;
          testpixel[1] = testy;

          // set ra,dec of zenith to be centre of the tangent plane
          double ra0 = lst;
          double dec0 = lat;
          // calc direction cosines of test pixel and associated ra,dec coordinates
          l = double(testpixel[0]-shape[0]/2) * res;
          m = double(testpixel[1]-shape[1]/2) * res;
          ra  = ra0 + atan2(l,( sqrt(1-l*l-m*m)*cos(dec0) - m*sin(dec0) ));
          dec = asin(m*cos(dec0) + sqrt(1-l*l-m*m)*sin(dec0));

          // generate the default primary beam, centred at the test position
          parset.add("primarybeam","SKA_LOW_PB");
          parset.add("primarybeam.SKA_LOW_PB.pointing.ra",std::to_string(ra));
          parset.add("primarybeam.SKA_LOW_PB.pointing.dec",std::to_string(dec));
          PrimaryBeam::ShPtr PB =  PrimaryBeamFactory::make(parset);
          cout <<"new PB at ra = "<<ra*12./casacore::C::pi<<" hrs, dec = "<<dec*180./casacore::C::pi<<" deg"<< endl;

          // get coordinates of the direction axes
          const int dcPos = coordsys.findCoordinate(casacore::Coordinate::DIRECTION,-1);
          const casacore::DirectionCoordinate outDC = coordsys.directionCoordinate(dcPos);

          casacore::Array<double> BeamArr(shape);

          for (int x=0; x<shape[0];++x) {
              for (int y=0; y<shape[1];++y) {

                  // get the current pixel location
                  pixel[0] = x;
                  pixel[1] = y;
                  l = double(pixel[0]-shape[0]/2) * res;
                  m = double(pixel[1]-shape[1]/2) * res;
                  n = sqrt(1. - l*l - m*m);
                  ra  = ra0 + atan2( l, n*cos(dec0) - m*sin(dec0) );
                  dec = asin( m*cos(dec0) + n*sin(dec0) );

                  casacore::Matrix<casacore::Complex> J = PB->getJones(ra,dec,frequency);

                  BeamArr(pixel) = real( J(0,0)*conj(J(0,0)) + J(0,1)*conj(J(0,1)) );

              }
          }

          cout << "testVal: " << BeamArr(testpixel) << endl;

          cout << "                    " << BeamArr(casacore::IPosition(2,testpixel[0]-1,testpixel[1]-1)) <<
                                    ", " << BeamArr(casacore::IPosition(2,testpixel[0]-1,testpixel[1]  )) <<
                                    ", " << BeamArr(casacore::IPosition(2,testpixel[0]-1,testpixel[1]+1)) << endl;
          cout << "testVal neighbours: " << BeamArr(casacore::IPosition(2,testpixel[0],  testpixel[1]-1)) <<
                                    ", " << BeamArr(casacore::IPosition(2,testpixel[0],  testpixel[1]  )) <<
                                    ", " << BeamArr(casacore::IPosition(2,testpixel[0],  testpixel[1]+1)) << endl;
          cout << "                    " << BeamArr(casacore::IPosition(2,testpixel[0]+1,testpixel[1]-1)) <<
                                    ", " << BeamArr(casacore::IPosition(2,testpixel[0]+1,testpixel[1]  )) <<
                                    ", " << BeamArr(casacore::IPosition(2,testpixel[0]+1,testpixel[1]+1)) << endl;

          // ensure that the beam is centred on the test pixel (e.g. 1 on pixel <1 off pixel)
          CPPUNIT_ASSERT(BeamArr(casacore::IPosition(2,testpixel[0]-1,testpixel[1]-1)) < 1 - 1e-7);
          CPPUNIT_ASSERT(BeamArr(casacore::IPosition(2,testpixel[0]-1,testpixel[1]  )) < 1 - 1e-7);
          CPPUNIT_ASSERT(BeamArr(casacore::IPosition(2,testpixel[0]-1,testpixel[1]+1)) < 1 - 1e-7);
          CPPUNIT_ASSERT(BeamArr(casacore::IPosition(2,testpixel[0],  testpixel[1]-1)) < 1 - 1e-7);
          CPPUNIT_ASSERT(BeamArr(casacore::IPosition(2,testpixel[0],  testpixel[1]  )) > 1 - 1e-7);
          CPPUNIT_ASSERT(BeamArr(casacore::IPosition(2,testpixel[0],  testpixel[1]+1)) < 1 - 1e-7);
          CPPUNIT_ASSERT(BeamArr(casacore::IPosition(2,testpixel[0]+1,testpixel[1]-1)) < 1 - 1e-7);
          CPPUNIT_ASSERT(BeamArr(casacore::IPosition(2,testpixel[0]+1,testpixel[1]  )) < 1 - 1e-7);
          CPPUNIT_ASSERT(BeamArr(casacore::IPosition(2,testpixel[0]+1,testpixel[1]+1)) < 1 - 1e-7);

          cout << "SKA_LOW_PB test complete" << endl;

      }
      void testCreateAbstract()
      {
          // PrimaryBeam is still an abstract class
          // calling createPriamryBeam static method should raise an
          // exception
          LOFAR::ParameterSet parset;
          PrimaryBeam::createPrimaryBeam(parset);

      }




  }; // class
  } // synthesis
} //askap
