/// @file SKA_LOW_PB.cc
/// @brief Primary Beam class for SKA-LOW
/// @details Implements the methods that evaluate the SKA-LOW primary beam gain
///
/// @copyright (c) 2020 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

#include "askap_imagemath.h"


#include "PrimaryBeam.h"
#include "SKA_LOW_PB.h"
#include <askap/askap/AskapError.h>
#include <askap/askap/AskapLogging.h>
#include <Common/ParameterSet.h>

#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/IPosition.h>

ASKAP_LOGGER(logger, ".primarybeam.skalowpb");
namespace askap {
    namespace imagemath {


            SKA_LOW_PB::SKA_LOW_PB() {
                ASKAPLOG_DEBUG_STR(logger,"SKA_LOW_PB default contructor");
            }


            SKA_LOW_PB::~SKA_LOW_PB() {
            }

            SKA_LOW_PB::SKA_LOW_PB(const SKA_LOW_PB &other) : PrimaryBeam(other) {
                ASKAPLOG_DEBUG_STR(logger,"SKA_LOW_PB copy contructor");
            }

            PrimaryBeam::ShPtr SKA_LOW_PB::createDefaultPrimaryBeam() {

               SKA_LOW_PB::ShPtr ptr;

               ptr.reset( new SKA_LOW_PB() );

               // Until the common SKA beam library is available, use a Gaussian place holder.
               // g = exp( - angular_offset**2 / (2*sigma**2) )
               // g = exp( - angular_offset**2 / (2*(FWHM/sqrt(8*log(2)))**2) )
               // g = exp( - 4*log(2) * angular_offset**2 / FWHM**2 )
               // g = exp( - 4*log(2) * angular_offset**2 / (c/freq/itsApertureSize)**2 )
               // g = exp( - 4*log(2)*itsApertureSize**2/c**2 * angular_offset**2 * freq**2 )
               ptr->itsApertureSize = 35.0;
               ptr->itsRA0 = 0.0;
               ptr->itsDec0 = 0.0;

               ASKAPLOG_DEBUG_STR(logger,"Created SKA_LOW PB with aperture = "<<ptr->itsApertureSize<< "ra,dec="<<ptr->itsRA0<<","<<ptr->itsDec0 );
               const double D_over_c = ptr->itsApertureSize / casacore::C::c;
               ptr->itsExpConst = -4*log(2)*D_over_c*D_over_c;

               ASKAPLOG_DEBUG_STR(logger,"Created Default SKA_LOW PB instance");
               return ptr;
            }

            PrimaryBeam::ShPtr SKA_LOW_PB::createPrimaryBeam(const LOFAR::ParameterSet &parset) {
               ASKAPLOG_DEBUG_STR(logger, "createPrimaryBeam for the SKA_LOW Primary Beam ");

               ASKAPLOG_WARN_STR(logger,"Creating SKA_LOW primary beam. "
                   "This model is under construction and liable to change without warning.");

               // this is static so use this to create the instance....

               SKA_LOW_PB::ShPtr ptr;

               // We need to pull all the parameters out of the parset - and set
               // all the private variables required to define the beam

               ptr.reset( new SKA_LOW_PB() );

               // Until the common SKA beam library is available, use a Gaussian place holder.
               ptr->itsApertureSize = parset.getDouble( "aperture", 35 );
               ptr->itsRA0          = parset.getDouble( "pointing.ra", 0 );
               ptr->itsDec0         = parset.getDouble( "pointing.dec", 0 );

               const double D_over_c = ptr->itsApertureSize / casacore::C::c;
               ptr->itsExpConst = -4*log(2)*D_over_c*D_over_c;

               ASKAPLOG_DEBUG_STR(logger,"Created SKA_LOW PB instance with aperture = "<<ptr->itsApertureSize<< "ra,dec="<<ptr->itsRA0<<","<<ptr->itsDec0 );
               return ptr;

            }

            double SKA_LOW_PB::evaluateAtOffset(double offsetDist, double frequency) {
                ASKAPLOG_WARN_STR(logger,"SKA_LOW_PB::evaluateAtOffset: unsupported option. Returning 1");
                return 1.0;
            }

            double SKA_LOW_PB::evaluateAtOffset(double offsetPAngle, double offsetDist, double frequency) {
                ASKAPLOG_WARN_STR(logger,"SKA_LOW_PB::evaluateAtOffset: unsupported option. Returning 1");
                return 1.0;
            }

            void SKA_LOW_PB::evaluateAlphaBetaAtOffset(float& alpha, float& beta,
                                                   double offsetPA, double offsetDist, double frequency) {
                ASKAPLOG_WARN_STR(logger,"SKA_PB::evaluateAlphaBetaAtOffset: unsupported option.");
            }
            double SKA_LOW_PB::getLeakageAtOffset(double offsetPAngle, double offsetDist, int pol, double frequency) {
                ASKAPLOG_WARN_STR(logger,"SKA_PB::getLeakageAtOffset: unsupported option.");
                return 0;
            }

            casacore::Matrix<casacore::Complex> SKA_LOW_PB::getJonesAtOffset(double offset, double frequency) {
                ASKAPLOG_WARN_STR(logger,"SKA_LOW_PB::getJonesAtOffset: unsupported option. Returning I");
                casacore::Matrix<casacore::Complex> Jones(2,2,0.0);
                Jones(0,0) = casacore::Complex(1.0,0.0);
                Jones(1,1) = Jones(0,0);
                return Jones;
            }

            casacore::Matrix<casacore::Complex> SKA_LOW_PB::getJones(double ra, double dec, double frequency) {

                ASKAPCHECK(dec >= -casacore::C::pi_2, "dec must not be less than -pi/2");
                ASKAPCHECK(dec <= casacore::C::pi_2, "dec must not be larger than pi/2");

                const double offset = acos( sin(dec)*sin(itsDec0) + cos(dec)*cos(itsDec0)*cos(ra-itsRA0) );

                double expVariable = frequency * offset;
                expVariable *= expVariable;

                casacore::Matrix<casacore::Complex> Jones(2,2,0.0);
                Jones(0,0) = exp( itsExpConst * expVariable );
                Jones(1,1) = Jones(0,0);

                return Jones;

            }

    }
}
