/// @file SKA_LOW_PB.h
/// @brief Primary Beam class for SKA-LOW
/// @details Implements the methods that evaluate the SKA-LOW primary beam gain
///
/// @copyright (c) 2020 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

#ifndef ASKAP_SKA_LOW_H
#define ASKAP_SKA_LOW_H

#include "PrimaryBeam.h"

#include <boost/shared_ptr.hpp>
#include <Common/ParameterSet.h>

#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/IPosition.h>

namespace askap {
namespace imagemath {

    class SKA_LOW_PB : public PrimaryBeam

    {

    public:

        typedef boost::shared_ptr<SKA_LOW_PB> ShPtr;

        SKA_LOW_PB();

        static inline std::string PrimaryBeamName() { return "SKA_LOW_PB";}

        virtual ~SKA_LOW_PB();

        SKA_LOW_PB(const SKA_LOW_PB &other);

        static PrimaryBeam::ShPtr createDefaultPrimaryBeam();

        static PrimaryBeam::ShPtr createPrimaryBeam(const LOFAR::ParameterSet &parset);

        /// Set some parameters

        // temporarilty here for the temporary Gaussian beam
        void setApertureSize(double apsize) { this->itsApertureSize = apsize; };
        void setExpConst(double expconst) { this->itsExpConst = expconst; };

        /// Get some parameters

        virtual double evaluateAtOffset(double offsetPA, double offsetDist, double frequency);
        virtual double evaluateAtOffset(double offsetDist, double frequency);
        virtual void evaluateAlphaBetaAtOffset(float& alpha, float& beta,
                                               double offsetPA, double offsetDist, double frequency);
        virtual double getLeakageAtOffset(double offsetPAngle, double offsetDist, int pol, double frequency);
        virtual casacore::Matrix<casacore::Complex> getJonesAtOffset(double offset, double frequency);
        virtual casacore::Matrix<casacore::Complex> getJones(double ra, double dec, double frequency);

    private:

        // Beam pointing and shape models will likely involve dipole delay
        // setting, calibration, etc. The plans is to use a common library
        // for this, to coordinate with other packages, so for now just use
        // a single Gaussian as a placeholder.

        // temporarily using a Gaussian. Just set pointing centre to track
        // a given RA,Dec (in radians)
        double itsRA0;
        double itsDec0;

        // A few other temporary parameters for the temporary Gaussian beam
        double itsApertureSize;
        double itsExpConst;

    };

} // namespace imagemath

} // namespace askap


#endif // #ifndef ASKAP_SKA_LOW_H
