/// @file ASKAP_PB.h
/// @brief Primary Beam class for ASKAP
/// @details Implements the methods that evaluate the ASKAP primary beam gain
///          for models more complicated than 2D gaussians
/// @copyright (c) 2020 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Mark Wieringa <mark.wieringa@csiro.au>

#ifndef ASKAP_ASKAP_PB_H
#define ASKAP_ASKAP_PB_H

#include "PrimaryBeam.h"

#include <boost/shared_ptr.hpp>
#include <Common/ParameterSet.h>

#include <casacore/casa/Arrays/Cube.h>
#include <casacore/scimath/Mathematics/Interpolate2D.h>
#include <string>

namespace askap {
namespace imagemath {

    class ASKAP_PB : public PrimaryBeam {

    public:

        typedef boost::shared_ptr<ASKAP_PB> ShPtr;

        ASKAP_PB();

        static inline std::string PrimaryBeamName() { return "ASKAP_PB";}

        virtual ~ASKAP_PB();

        ASKAP_PB(const ASKAP_PB &other);

        static PrimaryBeam::ShPtr createDefaultPrimaryBeam();

        /// @brief create and ASKAP primary beam from a parset specification
        /// @details The parset should specify an image parameter pointing at a 4D cube
        /// containing axes x,y, either frequency channel or taylor term, and beam number.
        /// The nterms parameter switches modes between spectral cube (nterms=0) or beam taylor terms (nterms>0)
        static PrimaryBeam::ShPtr createPrimaryBeam(const LOFAR::ParameterSet& parset);

        virtual double evaluateAtOffset(double offset, double frequency);

        virtual double evaluateAtOffset(double offsetPA, double offsetDist, double frequency);

        /// @brief get alpha and beta parameters of the beam at the given offset
        /// @details return the beam spectral index and curvature at the given offset
        /// @param[out] alpha, the spectral index of the beam
        /// @param[in/out] beta, the curvature of the beam, if beta<0 on entry,
        /// and the beam is given as a spectral cube, it is not calculated
        /// @param[in] offsetPA position angle of offset position (radians)
        /// @param[in] offsetDist angular distance of offset position (radians)
        /// @param[in] frequency the frequency to evaluate the alpha and beta parameters at (Hz)
        virtual void evaluateAlphaBetaAtOffset(float& alpha, float& beta, double offsetPA, double offsetDist, double frequency);

        /// @brief get leakage parameters of the beam at the given offset
        /// @details return the beam polarisation leakage (from I to pol) at the given offset for
        /// the specified polarisation. Note pol=0 will return same as evaluateAtOffset
        /// @param[out] alpha, the spectral index of the beam
        /// @param[in/out] beta, the curvature of the beam, if beta<0 on entry,
        /// and the beam is given as a spectral cube, it is not calculated
        /// @param[in] offsetPA position angle of offset position (radians)
        /// @param[in] offsetDist angular distance of offset position (radians)
        /// @param[in] pol polarisation to return: I=0, Q=1, U=2, V=3
        /// @param[in] frequency the frequency to evaluate the leakage at (Hz)
        virtual double getLeakageAtOffset(double offsetPAngle, double offsetDist, int pol, double frequency);

        virtual casacore::Matrix<casacore::Complex> getJonesAtOffset(double offset, double frequency);

        /// @brief obtain Jones matrix
        /// @details Sample primary beam model in a given direction.
        /// Definition of direction coords is dependent on inheriting class
        /// @param[in] dir1 1st direction coord in radians: ra (SKA_LOW_PB), az (MWA_PB)
        /// @param[in] dir2 2nd direction coord in radians: dec (SKA_LOW_PB), za (MWA_PB)
        /// @param[in] freq frequency in Hz
        virtual casacore::Matrix<casacore::Complex> getJones(double dir1, double dir2, double freq);

        /// get and set functions

        casacore::Matrix<casacore::Matrix<float> > getResponseArray() const
            { return response_p; }
        double getXscale() const
            { return xscale_p; }
        double getYscale() const
            { return yscale_p; }
        double getFreqOffset() const
            { return f0_p; }
        double getAlpha() const
            { return alpha_p; }
        double getFreqScale() const
            { return df_p; }
        std::string getInterpolationMethod() const
            { return iMethod_p; }
        bool getNterm() const
            { return nterm_p; }

        void setResponseArray(const casacore::Array<float>& response);

        void setXscale(double xscale)
            { xscale_p = xscale; }
        void setYscale(double yscale)
            { yscale_p = yscale; }
        void setXrefPix(double refPix)
            { x0_p = refPix;}
        void setYrefPix(double refPix)
            { y0_p = refPix;}
        // the rotation of the beam pattern wrt the meridian
        void setAlpha(double alpha)
            { alpha_p = alpha; }
        void setFreqOffset(double freq0)
            { f0_p = freq0; }
        void setFreqScale(double df)
            { df_p = df; }
        void setInterpolationMethod(std::string method)
            { iMethod_p = method; }
        void setNterm(int nterm)
            { nterm_p = nterm; }

    private:

        // Matrix with beam measurement or model images by frequency channel or taylor term and polarisation
        casacore::Matrix<casacore::Matrix<float> > response_p;
        // offset in x and y (assumed to be 0 at n/2 for version 1)
        double x0_p, y0_p;
        // scale in x and y (radians/pixel);
        double xscale_p, yscale_p;
        // rotation of beam response wrt meridian
        double alpha_p;
        // Freq offset (Hz) and scale (Hz/pixel)
        double f0_p, df_p;
        // Interpolation method
        std::string iMethod_p;
        casacore::Interpolate2D inter_p;
        // Number of taylor terms in beam model
        int nterm_p;
        // beam/feed pointing direction
        double itsRA0;
        double itsDec0;
        // taper the beam model to zero
        bool itsTaper;
        double itsTaperCutoff;
        // set leakage to zero on axis
        bool itsNormaliseLeakage;

    }; // class
} // imagemath
} //askap

#endif
