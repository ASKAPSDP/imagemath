/// @file ASKAP_PB.cc
/// @brief Primary Beam class for ASKAP
/// @details Implements the methods that evaluate the ASKAP primary beam gain
///
/// @copyright (c) 2020 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Mark Wieringa <mark.wieringa@csiro.au>

#include "askap_imagemath.h"


#include "PrimaryBeam.h"
#include "ASKAP_PB.h"
#include <askap/askap/AskapError.h>
#include <askap/askap/AskapLogging.h>
//#include <askap/imageaccess/ImageAccessFactory.h>
#include <casacore/images/Images/ImageUtilities.h>
#include <Common/ParameterSet.h>

ASKAP_LOGGER(logger, ".primarybeam.askappb");
namespace askap {
    namespace imagemath {


        ASKAP_PB::ASKAP_PB() {
            ASKAPLOG_DEBUG_STR(logger,"ASKAP_PB default contructor");
        }


        ASKAP_PB::~ASKAP_PB() {
        }

        ASKAP_PB::ASKAP_PB(const ASKAP_PB &other)
        {
            ASKAPLOG_DEBUG_STR(logger,"ASKAP_PB copy contructor");
        }

        PrimaryBeam::ShPtr ASKAP_PB::createPrimaryBeam(const LOFAR::ParameterSet &parset)
        {
           ASKAPLOG_DEBUG_STR(logger, "createPrimaryBeam for the ASKAP Primary Beam ");

           // this is static so use this to create the instance....

           // just for logging, declare private handle to avoid issues with template
           // log4cxx::LoggerPtr logger = log4cxx::Logger::getLogger(askap::generateLoggerName(std::string("createASKAP_PB")));
           //

           ASKAP_PB::ShPtr ptr;

           // We need to pull all the parameters out of the parset - and set
           // all the private variables required to define the beam

           ptr.reset( new ASKAP_PB());

           // Options - create from an image
           std::string imageName = parset.getString("image");
           // Select the beam we need
           int beam = parset.getInt("beam_number",0);
           ASKAPCHECK(beam>=0,"Invalid beam number");
           // work out if we have a Taylor term or frequency cube
           int nterm = parset.getInt("nterms",0);
           ASKAPCHECK(nterm<=3,"Maximum of 3 Taylor terms allowed");

           // initialise an image accessor
           //boost::shared_ptr<IImageAccess<casacore::Float> > iacc = ImageAccessFactory(parset);
           // Use casacore instead - cyclic dependencies appear when we use base-accessors from base-imagemath
           std::shared_ptr<casacore::ImageInterface<casacore::Float> > iacc = casacore::ImageUtilities::openImage<casacore::Float>(imageName);

           // get coordinate info
           casa::IPosition shape = iacc->shape();
           casacore::CoordinateSystem cs = iacc->coordinates();
           casacore::Vector<casacore::Double> refPix = cs.referencePixel();
           casacore::Vector<casacore::Double> refVal = cs.referenceValue();
           casacore::Vector<casacore::Double> inc = cs.increment();
           casacore::Vector<casacore::String> axisNames = cs.worldAxisNames();
           int ndim = shape.nelements();
           // do some checks: axes should be (old style) ra, dec, freq, [pol], beam or
           // (new style) ra, dec, freq, [taylor], pol, beam
           // If taylor is present, freq should be size 1 and pol is not optional
           // for old style files the freq axis can contain the taylor terms or the cube frequencies
           ASKAPCHECK(axisNames(0).startsWith("Right") && axisNames(1).startsWith("Decl") && axisNames(2).startsWith("Freq"),
                "First 3 axes of beam cube should be RA, Dec and Freq, you have: "<<axisNames(0)<<", "<<axisNames(1)<<", "<<axisNames(2));
           ASKAPCHECK(ndim>4 || axisNames(3).startsWith("BEAM"),"Fourth axis should be BEAM for 4 dimensional beam cube, you have: "<<axisNames(3));
           ASKAPCHECK(ndim<6 || (axisNames(3).startsWith("TAYLOR") && shape(2)==1),
                "Taylor axis should be 4th axis in 6D beam cube and Freq axis should have size 1, you have: "<<axisNames(3)<<", "<<shape(2));
           const int taylorAxis = ndim==6 ? 3 : 2;
           ASKAPCHECK(nterm<=shape(taylorAxis),"Not enough Taylor terms present in beam cube: nterm = "<<nterm<<", taylor axis size = "<<shape(taylorAxis));
           casacore::IPosition blc(ndim,0), trc(shape-1);
           blc(ndim-1)=beam; trc(ndim-1)=beam;
           if (ndim < 6 && (nterm==0 && shape(2)<4)) {
               ASKAPLOG_WARN_STR(logger,"nterms=0 implies a frequency cube beam model, but there are only "<<shape(2)<<" frequency planes");
           }
           // set parameters
           ptr->setNterm(nterm);
           ptr->setAlpha(parset.getDouble("alpha",0.0));
           // for x and y we assume refpix = n/2
           if (refPix(0) != shape(0)/2 || refPix(1) != shape(1)/2) {
               ASKAPLOG_WARN_STR(logger,"ASKAP Beam cube should have refpix at image centre, you have refpix = "<<refPix(0)<<", "<<refPix(1));
           }
           ptr->setXscale(parset.getDouble("xscale",inc(0)));
           ASKAPCHECK(ptr->getXscale() != 0,"xscale must be non zero");
           ptr->setYscale(parset.getDouble("yscale",inc(1)));
           ASKAPCHECK(ptr->getYscale() != 0,"yscale must be non zero");
           ptr->setXrefPix(refPix(0));
           ptr->setYrefPix(refPix(1));
           ptr->setFreqScale(parset.getDouble("freqscale",inc(2)));
           ASKAPCHECK(ptr->getFreqScale() != 0,"freqscale must non zero");
           ptr->setFreqOffset(parset.getDouble("freqoffset",refVal(2)-inc(2)*refPix(2)));
           ASKAPCHECK(ptr->getFreqOffset() > 0,"freqoffset must greater than zero");

           ptr->setInterpolationMethod(parset.getString("interpolation","linear"));
           ptr->inter_p = casacore::Interpolate2D(casacore::Interpolate2D::stringToMethod(ptr->getInterpolationMethod()));

           // for the Jones functions we need the feed pointing direction
           ptr->itsRA0          = parset.getDouble( "pointing.ra", 0 );
           ptr->itsDec0         = parset.getDouble( "pointing.dec", 0 );

           // If we're using the beam to create an illumination pattern, it may need to be tapered
           ptr->itsTaper        = parset.getBool("taper",false);
           const double cutoff  = parset.getDouble("taper.cutoff",0.04);
           // convert beam cutoff level to voltage response cutoff
           ptr->itsTaperCutoff  = (cutoff > 0 ? sqrt(cutoff) : 0.0);
           if (ptr->itsTaper) {
               ASKAPLOG_INFO_STR(logger,"Tapering the beam beyond cutoff level "<<cutoff);
           }
           ptr->itsNormaliseLeakage = parset.getBool("normaliseleakage",true);

           // load the response function
           if (ndim == 4) {
               // specify which axes we want to keep: ra, dec, either freq or taylor, and pol if present
               ptr->setResponseArray(iacc->getSlice(blc,trc-blc+1).nonDegenerate(casacore::IPosition(3,0,1,2)));
           } else if (ndim == 5) {
               ptr->setResponseArray(iacc->getSlice(blc,trc-blc+1).nonDegenerate(casacore::IPosition(4,0,1,2,3)));
           } else {
               ptr->setResponseArray(iacc->getSlice(blc,trc-blc+1).nonDegenerate(casacore::IPosition(4,0,1,3,4)));
           }

           ASKAPLOG_DEBUG_STR(logger,"Created ASKAP PB instance for beam number "<<beam<<" using "<< (nterm>0 ? "taylor term cube": "spectral cube")<< " nterms = "<<nterm<<" with x,y scale = "<<ptr->getXscale()<<", "<<
                ptr->getYscale()<<" and freq scale = "<< ptr->getFreqScale()<<", offset = "<<ptr->getFreqOffset()<<
            ", central value = "<<ptr->getResponseArray()(0,0)(refPix(0),refPix(1))<< " beam centre: "<<ptr->itsRA0 <<","<<ptr->itsDec0);

           return ptr;
        }

        void ASKAP_PB::setResponseArray(const casacore::Array<float>& response) {
            int ndim = response.ndim();
            ASKAPCHECK(ndim==3||ndim==4,"response array should have 3 or 4 dimensions");
            int nfreq = response.shape()(2);
            int npol = (ndim > 3 ? response.shape()(3) : 1);
            response_p.resize(nfreq,npol);
            casacore::IPosition blc(ndim,0);
            casacore::IPosition trc(response.shape()-1);
            for (int pol = 0; pol < npol; pol++) {
                if (ndim > 3) {
                    blc(3) = trc(3) = pol;
                }
                for (int freq = 0; freq < nfreq; freq++) {
                    blc(2) = trc(2) = freq;
                    response_p(freq,pol) = response(blc,trc).nonDegenerate(2);
                    // subtract on-axis leakage
                    if (itsNormaliseLeakage && pol > 0) {
                        response_p(freq,pol) -= response_p(freq,pol)(x0_p,y0_p);
                    }
                }
            }
        }

        double ASKAP_PB::evaluateAtOffset(double offsetDist,double frequency) {
            return evaluateAtOffset(0.0, offsetDist, frequency);
        }

        double ASKAP_PB::evaluateAtOffset(double offsetPAngle, double offsetDist, double frequency) {
            float pb = 0;

            // for Taylor terms: evaluate
            if (nterm_p>0) {
                // I think we need sin(offsetDist) here since we're going back to the image plane
                // Also the sin and cos are reversed compared to the GausianPB code
                double x = sin(offsetDist) * sin(offsetPAngle-alpha_p);
                double y = sin(offsetDist) * cos(offsetPAngle-alpha_p);

                casacore::Vector<casacore::Double> pix(2);
                pix(0) = x / xscale_p + x0_p;
                pix(1) = y / yscale_p + y0_p;

                double w = 1;
                for (int term = 0; term < nterm_p; term++) {
                    // interpolate
                    float pbt = 0;
                    inter_p.interp(pbt,pix,response_p(term,0));
                    if (term > 0) {
                        w *= (frequency - f0_p) / f0_p;
                        pbt *= w;
                    }
                    pb += pbt;
                }
            } else {
                // For frequency we pick the nearest plane and scale it
                int fplane = fmin(fmax(0, askap::nint((frequency - f0_p) / df_p)),response_p.nrow()-1);
                double responseFreq = f0_p + fplane * df_p;
                double scale = responseFreq / frequency;

                double x = sin(scale * offsetDist) * sin(offsetPAngle-alpha_p);
                double y = sin(scale * offsetDist) * cos(offsetPAngle-alpha_p);

                casacore::Vector<casacore::Double> pix(2);
                pix(0) = x / xscale_p + x0_p;
                pix(1) = y / yscale_p + y0_p;

                // interpolate
                inter_p.interp(pb,pix,response_p(fplane,0));
            }
            return pb;
        }

        void ASKAP_PB::evaluateAlphaBetaAtOffset(float& alpha, float& beta,
                                                 double offsetPAngle, double offsetDist, double frequency) {
            // for Taylor terms: evaluate
            if (nterm_p>0) {
                float pb=0;
                alpha=0;
                beta=0;
                double x = sin(offsetDist) * sin(offsetPAngle-alpha_p);
                double y = sin(offsetDist) * cos(offsetPAngle-alpha_p);

                casacore::Vector<casacore::Double> pix(2);
                pix(0) = x / xscale_p + x0_p;
                pix(1) = y / yscale_p + y0_p;

                for (int term = 0; term < nterm_p; term++) {
                    // interpolate
                    float pbt = 0, alphat=0;
                    inter_p.interp(pbt,pix,response_p(term,0));
                    if (term > 0) {
                        alphat = pbt;
                        double w = (frequency - f0_p) / f0_p;
                        pbt *= w;
                        if (term == 2) {
                            beta = alphat;
                            pbt *= w;
                            alphat *= w;
                        } // term>2 TBD?
                    }
                    pb += pbt;
                    alpha += alphat;
                }
                alpha /= pb;
                beta = beta/pb -alpha*(alpha-1)/2;
                //alpha = T1/T0;
                //beta = T2/T0 - alpha*(alpha-1)/2

            } else {
                // pick 3 planes closest to the frequency we want
                int nplane = response_p.nrow();
                ASKAPCHECK(nplane>1,"Cannot determine derivative of beam, need > 1 freq plane in beam model")
                ASKAPCHECK(beta < 0 || nplane>2,"Cannot determine 2nd derivative of beam, need > 2 freq planes in beam model")
                int fplane1 = fmin(fmax(0, askap::nint((frequency - f0_p) / df_p)),nplane-1);
                double responseFreq = f0_p + fplane1 * df_p;
                double scale = responseFreq / frequency;
                int fplane2 = fplane1 + 1;
                int fplane3 = fplane1 - 1;
                if (fplane2 >= nplane) {
                    fplane2 = nplane - 1;
                    fplane1--; fplane3--;
                }
                if (fplane3 < 0) {
                    fplane3 = 0 ;
                    fplane2++; fplane1++;
                }

                double x = sin(scale * offsetDist) * sin(offsetPAngle-alpha_p);
                double y = sin(scale * offsetDist) * cos(offsetPAngle-alpha_p);

                casacore::Vector<casacore::Double> pix(2);
                pix(0) = x / xscale_p + x0_p;
                pix(1) = y / yscale_p + y0_p;

                // interpolate
                float pb2 = 0;
                inter_p.interp(pb2,pix,response_p(fplane2,0));

                float pb3 = 0;
                inter_p.interp(pb3,pix,response_p(fplane3,0));

                alpha = 0;
                if (pb2 > 0 && pb3 > 0) {
                    double fdf = frequency/(df_p*(fplane2-fplane3));
                    alpha = (pb2-pb3)/((pb2+pb3)/2)*fdf;
                    if (beta >= 0) {
                        float pb1 = 0;
                        inter_p.interp(pb1,pix,response_p(fplane1,0));
                        if (pb1 > 0) {
                            fdf = frequency/df_p;
                            beta = (pb2 + pb3 - 2*pb1)/pb1*fdf*fdf/2 - alpha*(alpha-1)/2;
                        } else {
                            beta = 0;
                        }
                    }
                }
            }
        }

        double ASKAP_PB::getLeakageAtOffset(double offsetPAngle, double offsetDist, int pol, double frequency) {
          const int npol = response_p.ncolumn();
          ASKAPCHECK(npol > 1,"No polarisation leakage information in askap beam cube");
          ASKAPCHECK(pol < npol,"askap beam response does not have leakage for pol="<<pol);
          float pbl = 0;

          // if pol==1 or pol==2, we need both and return rotated linear pol correction - implement only for cubes
          //  as MFS Q,U is not very useful due to FR
          const bool doQU = (pol==1 || pol==2);

          // for Taylor terms: evaluate
          if (nterm_p>0) {
            double x = sin(offsetDist) * sin(offsetPAngle-alpha_p);
            double y = sin(offsetDist) * cos(offsetPAngle-alpha_p);

            casacore::Vector<casacore::Double> pix(2);
            pix(0) = x / xscale_p + x0_p;
            pix(1) = y / yscale_p + y0_p;

            double w = 1;
            for (int term = 0; term < nterm_p; term++) {
                // interpolate
                float pbt = 0;
                inter_p.interp(pbt,pix,response_p(term,pol));
                if (term > 0) {
                    w *= (frequency - f0_p) / f0_p;
                    pbt *= w;
                }
                pbl += pbt;
            }
          } else {
            // we pick the nearest plane and scale it
            int nplane = response_p.nrow();
            int fplane = fmin(fmax(0, askap::nint((frequency - f0_p) / df_p)),nplane-1);
            double responseFreq = f0_p + fplane * df_p;
            double scale = responseFreq / frequency;

            double x = sin(scale * offsetDist) * sin(offsetPAngle-alpha_p);
            double y = sin(scale * offsetDist) * cos(offsetPAngle-alpha_p);

            casacore::Vector<casacore::Double> pix(2);
            pix(0) = x / xscale_p + x0_p;
            pix(1) = y / yscale_p + y0_p;

            // interpolate
            if (doQU) {
                float pblQ = 0;
                float pblU = 0;
                inter_p.interp(pblQ,pix,response_p(fplane,1));
                inter_p.interp(pblU,pix,response_p(fplane,2));
                if (pol == 1) {
                    pbl = pblQ*cos(-2*alpha_p) + pblU*sin(-2*alpha_p);
                } else {
                    pbl = -pblQ*sin(-2*alpha_p) + pblU*cos(-2*alpha_p);
                }
            } else {
                inter_p.interp(pbl,pix,response_p(fplane,pol));
            }
          }
          return pbl;
        }

        casacore::Matrix<casacore::Complex> ASKAP_PB::getJonesAtOffset(double offset, double frequency) {

            casacore::Matrix<casacore::Complex> Jones(2,2,0.0);
            float pb =  evaluateAtOffset(0.0,offset,frequency);
            if (pb > 0) {
              pb = sqrt(pb);
              Jones(0,0) = Jones(1,1) = casacore::Complex(pb,0.0);
            }
            return Jones;
        }

        casacore::Matrix<casacore::Complex> ASKAP_PB::getJones(double ra, double dec, double frequency) {
            ASKAPCHECK(dec >= -casacore::C::pi_2, "dec must not be less than -pi/2");
            ASKAPCHECK(dec <= casacore::C::pi_2, "dec must not be larger than pi/2");

            const double diffRa = ra - itsRA0;
            const double sd0 = sin(itsDec0);
            const double cd0 = cos(itsDec0);
            const double sd = sin(dec);
            const double cd = cos(dec);
            const double s1 = cd * sin(diffRa);
            const double c1 = cd0 * sd - sd0 * cd * cos(diffRa);
            // CHECK SIGN of angle
            const double angle = (s1 !=0 || c1 != 0) ? atan2(s1,c1) : 0.0;
            const double offset = acos( sd * sd0 + cd * cd0 * cos(diffRa) );

            // 12m dish, gaussian beam
            static const double D_over_c = 12.0 / casacore::C::c;
            static const double expConst = -4*log(2)*D_over_c*D_over_c;

            casacore::Matrix<casacore::Complex> Jones(2,2,0.0);
            float pb =  evaluateAtOffset(angle,offset,frequency);
            if (pb > 0.) {
                pb = sqrt(pb); // turn into antenna based gain
                // Try tapering the measured beam
                if (itsTaper && itsTaperCutoff>0) {
                    double expVariable = frequency * offset;
                    expVariable *= expVariable;
                    double gb = exp( expConst * expVariable )/itsTaperCutoff;
                    if (gb < 1.0) {
                        pb *= gb * gb; // taper - single factor was not enough
                    }
                }
                Jones(0,0) = pb;
                Jones(1,1) = Jones(0,0);
            }
            return Jones;
        }
    }
}
