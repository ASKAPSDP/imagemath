/// @file MWA_PB.h
/// @brief Primary Beam class for MWA
/// @details Implements the methods that evaluate the MWA primary beam gain
///
/// @copyright (c) 2020 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

#ifndef ASKAP_MWA_H
#define ASKAP_MWA_H

#include "PrimaryBeam.h"

#include <boost/shared_ptr.hpp>
#include <Common/ParameterSet.h>

#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/IPosition.h>

namespace askap {
namespace imagemath {

    class MWA_PB : public PrimaryBeam

    {

    public:

        typedef boost::shared_ptr<MWA_PB> ShPtr;

        MWA_PB();

        static inline std::string PrimaryBeamName() { return "MWA_PB";}

        virtual ~MWA_PB();

        MWA_PB(const MWA_PB &other);

        static PrimaryBeam::ShPtr createDefaultPrimaryBeam();

        static PrimaryBeam::ShPtr createPrimaryBeam(const LOFAR::ParameterSet &parset);

        /// Get some parameters

        double getDipoleSeparation();
        double getDipoleHeight();

        virtual double evaluateAtOffset(double offsetPA, double offsetDist, double frequency);
        virtual double evaluateAtOffset(double offsetDist, double frequency);
        virtual void evaluateAlphaBetaAtOffset(float& alpha, float& beta,
                                               double offsetPA, double offsetDist, double frequency);
        virtual double getLeakageAtOffset(double offsetPAngle, double offsetDist, int pol, double frequency);
        virtual casacore::Matrix<casacore::Complex> getJonesAtOffset(double offset, double frequency);
        virtual casacore::Matrix<casacore::Complex> getJones(double azimuth, double zenithAngle,
                                                        double frequency);

        /// Some sets.

        void setLatitude(double lat) {latitude=lat;};
        void setLongitude(double lon) {longitude=lon;};

        void setDipoleSeparation(double sep) {DipoleSeparation=sep;};
        void setDipoleHeight(double hgt) {DipoleHeight=hgt;};

    private:

        // Location of the MWA in radians
        double latitude;
        double longitude;

        // Number of dipole rows and columns in MWA tiles
        int numDipoleRows;
        int numDipoleColumns;

        // Separation of MWA dipoles in metres ("Short-Wide" tiles)
        double DipoleSeparation;

        // Height of MWA dipoles in metres ("Short-Wide" tiles)
        double DipoleHeight;

    };

} // namespace imagemath

} // namespace askap


#endif // #ifndef ASKAP_MWA_H
