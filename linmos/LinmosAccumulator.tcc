/// @file LinmosAccumulator.tcc
///
/// @brief combine a number of images as a linear mosaic
/// @details
///
/// @copyright (c) 2012,2014,2015 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

// other 3rd party
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <casacore/casa/Arrays/Array.h>
//#include <casacore/images/Images/ImageRegrid.h>
#include <casacore/lattices/LatticeMath/LatticeMathUtil.h>

// ASKAPsoft includes
#include <askap/askap/Application.h>
#include <askap/askap/AskapError.h>
#include <askap/askap/AskapLogging.h>
#include <askap/askap/AskapUtil.h>
#include <askap/askap/StatReporter.h>
#include <askap/imagemath/utils/MultiDimArrayPlaneIter.h>
#include <askap/imagemath/primarybeam/PrimaryBeam.h>
#include <askap/imagemath/primarybeam/GaussianPB.h>
#include <askap/imagemath/primarybeam/PrimaryBeamFactory.h>

#include "askap/imagemath/utils/ImagemathUtils.h"


ASKAP_LOGGER(linmoslogger, ".linmosaccumulator");

using namespace casa;


// variables functions used by the linmos accumulator class

// See loadParset for these options.
enum weight_types {FROM_WEIGHT_IMAGES=0, FROM_BP_MODEL, COMBINED};
// FROM_WEIGHT_IMAGES   Obtain pixel weights from weight images (parset "weights" entries)
// FROM_BP_MODEL        Generate pixel weights using a Gaussian primary-beam model
// COMBINED             The combined case - where we use both the weight image and the PB model
enum weight_states {CORRECTED=0, INHERENT, WEIGHTED};
// CORRECTED            Direction-dependent beams/weights have been divided out of input images
// INHERENT             Input images retain the natural primary-beam weighting of the visibilities
// WEIGHTED             Input images have full primary-beam-squared weighting

namespace askap {

    namespace imagemath {

        /// @brief Base class supporting linear mosaics (linmos)

        template<typename T>
        LinmosAccumulator<T>::LinmosAccumulator() : itsMethod("linear"),
                                                    itsDecimate(3),
                                                    itsReplicate(false),
                                                    itsForce(false),
                                                    itsOutputRef(-1),
                                                    itsOutputProjectionType(Projection::N_PROJ),
                                                    itsOutputDirType(MDirection::N_Types),
                                                    itsOutputCentreDefined(false),
                                                    itsWeightType(-1),
                                                    itsWeightState(-1),
                                                    itsNumTaylorTerms(-1),
                                                    itsCutoff(0.01),
                                                    itsMosaicTag("linmos"),
                                                    itsTaylorTag("taylor.0"),
                                                    itsUseWtLog(false),
                                                    itsUseWtFromHdr(false) {}

        // functions in the linmos accumulator class

        template<typename T>
        bool LinmosAccumulator<T>::loadParset(const LOFAR::ParameterSet &parset) {

            const std::vector<std::string> inImgNames = parset.getStringVector("names", true);
            std::vector<std::string> inWgtNames = parset.getStringVector("weights", std::vector<std::string>(), true);
            const std::vector<std::string> inStokesINames = parset.getStringVector("stokesinames", std::vector<std::string>(), true);
            const std::vector<int> inBeams = parset.getIntVector("beams",std::vector<int>(), true);
            const std::vector<double> inBeamAngles = parset.getDoubleVector("beamangles",std::vector<double>(), true);
            const std::string weightTypeName = parset.getString("weighttype");
            const std::string weightStateName = parset.getString("weightstate", "Corrected");

            itsDoLeakage = parset.getBool("removeleakage",false);
            const bool findMosaics = parset.getBool("findmosaics", false);

            // Check the input images
            ASKAPCHECK(inImgNames.size()>0, "Number of input images should be greater than 0");

            itsCentres.resize(inImgNames.size());

            // Check weighting options. One of the following must be set:
            //  - weightTypeName==FromWeightImages: get weights from input weight images
            //    * the number of weight images and their shapes must match the input images
            //  - weightTypeName==FromPrimaryBeamModel: set weights using a Gaussian beam model
            //    * the direction coordinate centre will be used as beam centre, unless ...
            //    * an output weight image will be written, so an output file name is required

            if (boost::iequals(weightTypeName, "FromWeightImages")) {
                itsWeightType = FROM_WEIGHT_IMAGES;
                ASKAPLOG_INFO_STR(linmoslogger, "Weights are coming from weight images");
            } else if (boost::iequals(weightTypeName, "FromPrimaryBeamModel")) {
                itsWeightType = FROM_BP_MODEL;
                ASKAPLOG_INFO_STR(linmoslogger, "Weights to be set using a primary-beam models");
            } else if (boost::iequals(weightTypeName, "Combined")) {
                  itsWeightType = COMBINED;
                  ASKAPLOG_INFO_STR(linmoslogger, "Weights to be set using a combination of weight images and primary-beam models");
            } else {
                ASKAPLOG_ERROR_STR(linmoslogger, "Unknown weighttype " << weightTypeName);
                return false;
            }

            if (findMosaics) {

                ASKAPLOG_INFO_STR(linmoslogger, "Image names to be automatically generated. Searching...");
                // check for useless parameters
                if (parset.isDefined("outname") || parset.isDefined("outweight")) {
                    ASKAPLOG_WARN_STR(linmoslogger, "  - output file names are specified in parset but ignored.");
                }
                if (parset.isDefined("nterms")) {
                    ASKAPLOG_WARN_STR(linmoslogger, "  - nterms is specified in parset but ignored.");
                }

                findAndSetMosaics(inImgNames);

                ASKAPCHECK(itsInImgNameVecs.size() > 0, "No suitable mosaics found.");
                ASKAPLOG_INFO_STR(linmoslogger, itsInImgNameVecs.size() << " suitable mosaics found.");

            } else {

                std::string outImgName = parset.getString("outname");
                std::string outWgtName = parset.getString("outweight");

                if (itsWeightType == FROM_WEIGHT_IMAGES || itsWeightType == COMBINED) {
                  if (inWgtNames.size() > 0) {
                    ASKAPCHECK(inImgNames.size()==inWgtNames.size(), "# weight images should equal # images");
                    // Now check if we are using images or weightslog files, no mixing the two
                    itsUseWtLog = (inWgtNames[0].find(".txt")!=string::npos);
                    for (auto i : inWgtNames) {
                        ASKAPCHECK((i.find(".txt")!=string::npos)==itsUseWtLog,
                        "All weights arguments need to be of the same type (image or log)");
                    }
                    if (parset.isDefined("useweightslog")) {
                        itsUseWtLog = parset.getBool("useweightslog");
                        if (useWeightsLog()) {
                            for (int i=0; i<inWgtNames.size(); i++) {
                                // check if we need to append .txt
                                if (inWgtNames[i].find(".txt") != inWgtNames[i].size()-4) {
                                    inWgtNames[i].append(".txt");
                                }
                            }
                            ASKAPLOG_INFO_STR(linmoslogger,"weights files: "<<inWgtNames);
                        }
                    }
                    if (useWeightsLog()) {
                        ASKAPLOG_INFO_STR(linmoslogger,"Using weightslog files instead of weights images");
                    }
                  } else {
                    // No weights images - assume we read the weights from header/extension
                    itsUseWtFromHdr = true;
                  }
                }

                // Check for taylor terms

                if (parset.isDefined("nterms")) {

                    itsNumTaylorTerms = parset.getInt32("nterms");
                    findAndSetTaylorTerms(inImgNames, inWgtNames, outImgName, outWgtName);

                } else {

                    setSingleMosaic(inImgNames, inWgtNames, inStokesINames, outImgName, outWgtName);

                }

            }

            if (itsWeightType == FROM_WEIGHT_IMAGES) {

                // if reading weights from images, check for inputs associated with other kinds of weighting
                if (parset.isDefined("feeds.centre") ||
                    parset.isDefined("feeds.centreref") ||
                    parset.isDefined("feeds.offsetsfile") ||
                    parset.isDefined("feeds.names") ||
                    parset.isDefined("feeds.spacing") ) {
                    ASKAPLOG_WARN_STR(linmoslogger,
                        "Beam information specified in parset but ignored. Using weight images");
                }

            } else if (itsWeightType == FROM_BP_MODEL) {

                // check for inputs associated with other kinds of weighting
                if (inWgtNames.size()>0) {
                    ASKAPLOG_WARN_STR(linmoslogger,
                        "Weight images specified in parset but ignored. Using a primary-beam model");
                }

            }

            // Check the initial weighting state of the input images

            if (boost::iequals(weightStateName, "Corrected")) {
                ASKAPLOG_INFO_STR(linmoslogger,
                        "Input image state: Direction-dependent beams/weights have been divided out");
                itsWeightState = CORRECTED;
            } else if (boost::iequals(weightStateName, "Inherent")) {
                ASKAPLOG_INFO_STR(linmoslogger,
                        "Input image state: natural primary-beam weighting of the visibilities is retained");
                itsWeightState = INHERENT;
            } else if (boost::iequals(weightStateName, "Weighted")) {
                ASKAPLOG_INFO_STR(linmoslogger,
                        "Input image state: full primary-beam-squared weighting");
                itsWeightState = WEIGHTED;
            } else {
                ASKAPLOG_ERROR_STR(linmoslogger,
                        "Unknown weightstyle " << weightStateName);
                return false;
            }

            if (parset.isDefined("cutoff")) itsCutoff = parset.getFloat("cutoff");

            if (parset.isDefined("regrid.method")) itsMethod = parset.getString("regrid.method");
            if (parset.isDefined("regrid.decimate")) itsDecimate = parset.getInt("regrid.decimate");
            if (parset.isDefined("regrid.replicate")) itsReplicate = parset.getBool("regrid.replicate");
            if (parset.isDefined("regrid.force")) itsForce = parset.getBool("regrid.force");

            if (parset.isDefined("psfref")) {
                ASKAPCHECK(parset.getUint("psfref")<inImgNames.size(), "PSF reference-image number is too large");
            }

            itsOutputRef = parset.getInt("outputref",-1);
            ASKAPCHECK(itsOutputRef<int(inImgNames.size()), "Output reference-image number is too large");

            // outputcentre parameters 2 and 3 set output direction type and projection if present
            const vector<string> centre(parset.getStringVector("outputcentre",std::vector<std::string>(), true));
            if (centre.size()>3) {
              Projection::Type pType = Projection::type(centre[3]);
              ASKAPCHECK(pType!=Projection::N_PROJ,"Unknown projection type specified: "<<centre[3]);
              itsOutputProjectionType = pType;
            }
            if (centre.size()>2) {
              ASKAPCHECK(MDirection::getType(itsOutputDirType,centre[2]),"Unknown MDirection type specified: "<<centre[2]);
              ASKAPLOG_INFO_STR(linmoslogger, "Using output coordinate type: "<<MDirection::showType(itsOutputDirType)<<", projection:"<<Projection::name(itsOutputProjectionType));
            }

            if (itsOutputRef<0) {
                if (centre.size() == 0) {
                    ASKAPLOG_INFO_STR(linmoslogger,"Using a central field as the output coordinate reference");
                } else {
                    ASKAPCHECK(centre.size()>1, " -> the outputcentre parameter should have 2 or more elements");
                    if (centre[0].size() > 0 && centre[1].size() > 0) {
                        if (itsOutputDirType == MDirection::N_Types) {
                            itsOutputDirType = MDirection::J2000;
                        }
                        if (itsOutputProjectionType == Projection::N_PROJ) {
                            itsOutputProjectionType = Projection::SIN;
                        }
                        itsOutputCentre = MDirection(convertDir(centre[0], centre[1]),itsOutputDirType);
                        ASKAPLOG_INFO_STR(linmoslogger, "Using output centre: "<<itsOutputCentre.toString()<<", projection: "<<Projection::name(itsOutputProjectionType));
                        itsOutputCentreDefined = true;
                    }
                }
            } else {
                ASKAPLOG_INFO_STR(linmoslogger,"Using field "<<inImgNames[itsOutputRef]<< " as the output coordinate reference");

            }

            // sort out primary beam(s)
            itsPBs.clear();
            ASKAPLOG_INFO_STR(linmoslogger,"primarybeam = "<<parset.getString("primarybeam","GaussianPB"));
            if (parset.getString("primarybeam","GaussianPB")=="ASKAP_PB") {
                ASKAPLOG_DEBUG_STR(linmoslogger,"making ASKAP PB");
                // for ASKAP_PB we create a separate PB per input image,
                int n = inImgNames.size();
                LOFAR::ParameterSet myparset = parset;
                for (int i=0; i<n; i++) {
                    if (inBeams.size()==n) {
                        //use specified beam numbers
                        myparset.replace("primarybeam.ASKAP_PB.beam_number",
                            std::to_string(inBeams[i]));
                    } else {
                        // reading the beam number from the imagename
                        myparset.replace("primarybeam.ASKAP_PB.beam_number",
                            std::to_string(getBeamFromImageName(inImgNames[i])));
                    }
                    if (inBeamAngles.size()==n) {
                        // use specified beam angles
                        myparset.replace("primarybeam.ASKAP_PB.alpha",
                            std::to_string(inBeamAngles[i]));
                    }
                    ASKAPLOG_DEBUG_STR(linmoslogger,"primarybeam parset= "<<myparset);
                    itsPBs.push_back(PrimaryBeamFactory::make(myparset));
                }
            } else {
                itsPBs.push_back(PrimaryBeamFactory::make(parset));
            }
            return true;

        }

        template<typename T>
        void LinmosAccumulator<T>::setSingleMosaic(const std::vector<std::string> &inImgNames,
                                                   const std::vector<std::string> &inWgtNames,
                                                   const std::vector<std::string> &inStokesINames,
                                                   const std::string &outImgName,
                                                   const std::string &outWgtName) {

            // set some variables for the sensitivity image searches
            std::string image_tag = "image", restored_tag = ".restored", tmpName;
            // set false if any sensitivity images are missing or if not an image* mosaic
            itsDoSensitivity = true;

            // Check the input images
            for (size_t img = 0; img < inImgNames.size(); ++img) {

                // make sure the output image will not be overwritten
                ASKAPCHECK(inImgNames[img]!=outImgName,
                    "Output image, "<<outImgName<<", is present among the inputs");

                // if this is an "image*" file, see if there is an appropriate sensitivity image
                if (itsDoSensitivity) {
                    tmpName = inImgNames[img];
                    size_t image_pos = tmpName.find(image_tag);
                    // if the file starts with image_tag, look for a sensitivity image
                    if (image_pos == 0) {
                        tmpName.replace(image_pos, image_tag.length(), "sensitivity");
                        // remove any ".restored" sub-string from the file name
                        size_t restored_pos = tmpName.find(restored_tag);
                        if (restored_pos != std::string::npos) {
                            tmpName.replace(restored_pos, restored_tag.length(), "");
                        }
                        if (boost::filesystem::exists(tmpName)) {
                            itsInSenNameVecs[outImgName].push_back(tmpName);
                        } else {
                            ASKAPLOG_WARN_STR(linmoslogger,
                                "Cannot find file "<<tmpName<<" . Ignoring sensitivities.");
                            itsDoSensitivity = false;
                        }
                    } else {
                        ASKAPLOG_WARN_STR(linmoslogger,
                            "Input not an image* file. Ignoring sensitivities.");
                        itsDoSensitivity = false;
                    }
                }

            }

            // set a single key for the various file-name maps
            itsOutWgtNames[outImgName] = outWgtName;
            itsInImgNameVecs[outImgName] = inImgNames;
            if (itsWeightType == FROM_WEIGHT_IMAGES || itsWeightType == COMBINED) {
                itsInWgtNameVecs[outImgName] = inWgtNames;
            }
            if (itsDoSensitivity) {
                itsGenSensitivityImage[outImgName] = true;
                // set an output sensitivity file name
                tmpName = outImgName;
                tmpName.replace(0, image_tag.length(), "sensitivity");
                // remove any ".restored" sub-string from the weights file name
                size_t restored_pos = tmpName.find(restored_tag);
                if (restored_pos != std::string::npos) {
                    tmpName.replace(restored_pos, restored_tag.length(), "");
                }
                itsOutSenNames[outImgName] = tmpName;
            } else {
                itsGenSensitivityImage[outImgName] = false;
                // if some but not all sensitivity images were found, remove this key from itsInSenNameVecs
                if (itsInSenNameVecs.find(outImgName)!=itsInSenNameVecs.end()) {
                    itsInSenNameVecs.erase(outImgName);
                }
            }

            if (doLeakage() && inStokesINames.size()>0) {
                ASKAPCHECK(inStokesINames.size() == inImgNames.size(),
                "Number of Stokes I images should match number of input images");
                itsInStokesINameVecs[outImgName] = inStokesINames;
            }

        } // LinmosAccumulator<T>::setSingleMosaic()

        template<typename T>
        void LinmosAccumulator<T>::findAndSetTaylorTerms(const std::vector<std::string> &inImgNames,
                                                         const std::vector<std::string> &inWgtNames,
                                                         const std::string &outImgNameOrig,
                                                         const std::string &outWgtNameOrig) {

            ASKAPLOG_INFO_STR(linmoslogger, "Looking for "<<itsNumTaylorTerms<<" taylor terms");
            ASKAPCHECK(itsNumTaylorTerms>=0, "Number of taylor terms should be greater than or equal to 0");

            size_t pos0, pos1;
            pos0 = outImgNameOrig.find(itsTaylorTag);
            ASKAPCHECK(pos0!=std::string::npos, "Cannot find "<<itsTaylorTag<<" in output file "<<outImgNameOrig);
            pos1 = outImgNameOrig.find(itsTaylorTag, pos0+1); // make sure there aren't multiple entries.
            ASKAPCHECK(pos1==std::string::npos,
                "There are multiple  "<<itsTaylorTag<<" strings in output file "<<outImgNameOrig);

            // set some variables for the sensitivity image searches
            std::string image_tag = "image", restored_tag = ".restored", tmpName;
            itsDoSensitivity = true; // set false if any sensitivity images are missing or if not an image* mosaic

            for (int n = 0; n < itsNumTaylorTerms; ++n) {

                std::string outImgName = outImgNameOrig;
                std::string outWgtName = outWgtNameOrig;
                const std::string taylorN = "taylor." + std::to_string(n);

                // set a new key for the various output file-name maps
                outImgName.replace(outImgName.find(itsTaylorTag), itsTaylorTag.length(), taylorN);
                outWgtName.replace(outWgtName.find(itsTaylorTag), itsTaylorTag.length(), taylorN);
                itsOutWgtNames[outImgName] = outWgtName;

                for (uint img = 0; img < inImgNames.size(); ++img) {

                    // do some tests
                    std::string inImgName = inImgNames[img]; // short cut
                    pos0 = inImgName.find(itsTaylorTag);
                    ASKAPCHECK(pos0!=std::string::npos, "Cannot find "<<itsTaylorTag<<" in input file "<<inImgName);
                    pos1 = inImgName.find(itsTaylorTag, pos0+1); // make sure there aren't multiple entries.
                    ASKAPCHECK(pos1==std::string::npos,
                        "There are multiple "<<itsTaylorTag<<" strings in input file "<<inImgName);

                    // set a new key for the input file-name-vector map
                    inImgName.replace(pos0, itsTaylorTag.length(), taylorN);
                    itsInImgNameVecs[outImgName].push_back(inImgName);

                    // Check the input image
                    ASKAPCHECK(inImgName!=outImgName,
                        "Output image, "<<outImgName<<", is present among the inputs");

                    if (itsWeightType == FROM_WEIGHT_IMAGES || itsWeightType == COMBINED) {
                        if (!useWtFromHdr()) {
                            // do some tests
                            std::string inWgtName = inWgtNames[img]; // short cut
                            pos0 = inWgtName.find(itsTaylorTag);
                            ASKAPCHECK(pos0!=std::string::npos,
                            "Cannot find "<<itsTaylorTag<< " in input weight file "<<inWgtName);
                            pos1 = inWgtName.find(itsTaylorTag, pos0+1); // make sure there aren't multiple entries.
                            ASKAPCHECK(pos1==std::string::npos, "There are multiple " << itsTaylorTag <<
                                                           " strings in input file "<<inWgtName);

                            // set a new key for the input weights file-name-vector map
                            if (!useWeightsLog()) {
                                inWgtName.replace(pos0, itsTaylorTag.length(), taylorN);
                            }
                            itsInWgtNameVecs[outImgName].push_back(inWgtName);

                            // Check the input weights image
                            ASKAPCHECK(inWgtName!=outWgtName,
                                "Output wgt image, "<<outWgtName<<", is among the inputs");
                        }
                    }

                    // if this is an "image*" file, see if there is an appropriate sensitivity image
                    if (itsDoSensitivity) {
                        tmpName = inImgName;
                        size_t image_pos = tmpName.find(image_tag);
                        // if the file starts with image_tag, look for a sensitivity image
                        if (image_pos == 0) {
                            tmpName.replace(image_pos, image_tag.length(), "sensitivity");
                            // remove any ".restored" sub-string from the file name
                            size_t restored_pos = tmpName.find(restored_tag);
                            if (restored_pos != std::string::npos) {
                                tmpName.replace(restored_pos, restored_tag.length(), "");
                            }
                            if (boost::filesystem::exists(tmpName)) {
                                itsInSenNameVecs[outImgName].push_back(tmpName);
                            } else {
                                ASKAPLOG_WARN_STR(linmoslogger,
                                    "Cannot find file "<<tmpName<<" . Ignoring sensitivities.");
                                itsDoSensitivity = false;
                            }
                        } else {
                            ASKAPLOG_WARN_STR(linmoslogger,
                                "Input not an image* file. Ignoring sensitivities.");
                            itsDoSensitivity = false;
                        }

                    }
                    ASKAPLOG_INFO_STR(linmoslogger,"Taylor Image: " << inImgName);
                } // img loop (input image)

                // check whether any sensitivity images were found
                if (itsDoSensitivity) {
                    itsGenSensitivityImage[outImgName] = true;
                    // set an output sensitivity file name
                    tmpName = outImgName;
                    tmpName.replace(0, image_tag.length(), "sensitivity");
                    // remove any ".restored" sub-string from the weights file name
                    size_t restored_pos = tmpName.find(restored_tag);
                    if (restored_pos != std::string::npos) {
                        tmpName.replace(restored_pos, restored_tag.length(), "");
                    }
                    itsOutSenNames[outImgName] = tmpName;
                } else {
                    itsGenSensitivityImage[outImgName] = false;
                    // if some but not all sensitivity images were found, remove this key
                    if (itsInSenNameVecs.find(outImgName)!=itsInSenNameVecs.end()) {
                        itsInSenNameVecs.erase(outImgName);
                    }
                }

            } // n loop (taylor term)


        } // void LinmosAccumulator<T>::findAndSetTaylorTerms()

        template<typename T>
        void LinmosAccumulator<T>::removeBeamFromTaylorTerms(Array<T>& taylor0,
            Array<T>& taylor1,
            Array<T>& taylor2,
            const IPosition& curpos,
            const CoordinateSystem& inSys)
        {

            // The basics of this are we need to remove the effect of the beam
            // from the Taylor terms
            // This is only required if you do not grid with the beam. One wonders
            // whether we should just implement the beam (A) projection in the gridding.

            // This means redistribution of some of Taylor terms (tt) into tt'
            //
            // tt0' = tt0 - no change
            // tt1' = tt1 - (tt0 x alpha)
            // tt2' = tt2 - tt1 x alpha - tt0 x (beta - alpha(alpha + 1.)/2. )
            //
            // The assumption is that we rescale each constituent image. But we do need to
            // group them.

            // We need the Taylor terms for each pointing grouped together.
            // So lets just get those first

            // Timer for this routine
            // double timerStart = MPI_Wtime();
            // Can we safely assume taylor terms are just single planes?
            ASKAPASSERT(taylor0.shape()[2]<=1 && taylor0.shape()[3]<=1);

            // get coordinates of the spectral axis and the current frequency
            const int scPos = inSys.findCoordinate(Coordinate::SPECTRAL,-1);
            const SpectralCoordinate inSC = inSys.spectralCoordinate(scPos);
            int chPos = inSys.pixelAxes(scPos)[0];
            const T freq = inSC.referenceValue()[0] +
            (curpos[chPos] - inSC.referencePixel()[0]) * inSC.increment()[0];

            // get coordinates of the direction axes
            const int dcPos = inSys.findCoordinate(Coordinate::DIRECTION,-1);
            const DirectionCoordinate inDC = inSys.directionCoordinate(dcPos);

            // we need to interate through each of the taylor term images for all of the output
            // mosaics

            //
            // step through the pixels
            //
            #pragma omp parallel
            {
                const bool doT1 = (taylor1.size()>0);
                const bool doT2 = (taylor2.size()>0);
                if (doT1) {

                    const double refx = inDC.referencePixel()[0];
                    const double refy = inDC.referencePixel()[1];

                    const double incx = inDC.increment()[0];
                    const double incy = inDC.increment()[1];

                    const int nx = taylor0.shape()[0];
                    const int ny = taylor0.shape()[1];

                    // This should be safe, taylor arrays are created contiguous by image acessor
                    T* pT0 = taylor0.data();
                    T* pT1 = taylor1.data();
                    T* pT2 = taylor2.data();

                    #pragma omp for
                    for (int y=0; y < ny; ++y) {
                        size_t i = y * nx;
                        const double offsety = incy * double(y-refy);
                        const double offsety2 = offsety * offsety;
                        for (int x=0; x < nx; ++x, ++i) {

                            double offsetx = incx * double(x-refx);
                            double offsetBeam = asin( sqrt( offsetx * offsetx + offsety2 ) );
                            double offsetAngle = atan2(offsetx,offsety);

                            // get alpha and (if needed) beta
                            // this assumes that the reference frequency is the current
                            // frequency.
                            float alpha = 0;
                            // if beta<0 on input, evaluate can skip beta calculations if not needed
                            float beta = (doT2 ? 0: -1);
                            itsInPB->evaluateAlphaBetaAtOffset(alpha,beta,offsetAngle,offsetBeam,freq);

                            T t0 = pT0[i];
                            T t1 = pT1[i];

                            pT1[i] -= t0 * alpha;

                            if (doT2){
                                pT2[i] -= t1 * alpha + t0 * (beta - alpha * (alpha + 1)/2.);
                            }
                        }
                    } // for all pixels
                } // doT1
            } // omp parallel
            //double timerStop = MPI_Wtime();
            //ASKAPLOG_INFO_STR(linmoslogger,"removeBeamFromTaylorTerms took "<< timerStop-timerStart<<"s");
        } // removeBeamFromTaylorTerms()

        template<typename T>
        void LinmosAccumulator<T>::removeLeakage(Array<T>& polIm,
            Array<T>& stokesI, int pol, const IPosition& curpos,
            const CoordinateSystem& inSys)
        {
            // Timer for this routine
            // double timerStart = MPI_Wtime();

            // get coordinates of the spectral axis and the current frequency
            const int scPos = inSys.findCoordinate(Coordinate::SPECTRAL,-1);
            const SpectralCoordinate inSC = inSys.spectralCoordinate(scPos);
            int chPos = inSys.pixelAxes(scPos)[0];
            const T freq = inSC.referenceValue()[0] +
            (curpos[chPos] - inSC.referencePixel()[0]) * inSC.increment()[0];

            // get coordinates of the direction axes
            const int dcPos = inSys.findCoordinate(Coordinate::DIRECTION,-1);
            const DirectionCoordinate inDC = inSys.directionCoordinate(dcPos);

            // do a test call to avoid failing in many threads
            itsInPB->getLeakageAtOffset(0.0,0.0,pol,freq);

            // step through the pixels
            #pragma omp parallel
            {

                const double refx = inDC.referencePixel()[0];
                const double refy = inDC.referencePixel()[1];

                const double incx = inDC.increment()[0];
                const double incy = inDC.increment()[1];

                const int nx = polIm.shape()[0];
                const int ny = polIm.shape()[1];

                // This should be safe, arrays are created contiguous by image acessor
                T* pIm = polIm.data();
                T* pStokesI = stokesI.data();

                #pragma omp for
                for (int y=0; y < ny; ++y) {
                    size_t i = y * nx;
                    const double offsety = incy * double(y-refy);
                    const double offsety2 = offsety * offsety;
                    for (int x=0; x < nx; ++x, ++i) {

                        double offsetx = incx * double(x-refx);
                        double offsetBeam = asin( sqrt( offsetx * offsetx + offsety2 ) );
                        double offsetAngle = atan2(offsetx,offsety);

                        float leakage = itsInPB->getLeakageAtOffset(offsetAngle,offsetBeam,pol,freq);

                        pIm[i] -= leakage * pStokesI[i];
                    }
                } // for all pixels
            } // omp parallel
            //double timerStop = MPI_Wtime();
            //ASKAPLOG_INFO_STR(linmoslogger,"removeLeakage took "<< timerStop-timerStart<<"s");
        } // removeLeakage()

        template<typename T>
        void LinmosAccumulator<T>::findAndSetMosaics(const std::vector<std::string> &imageTags) {

            std::vector<std::string> prefixes;
            prefixes.push_back("image");
            prefixes.push_back("residual");
            //prefixes.push_back("weights"); // these need to be handled separately
            //prefixes.push_back("sensitivity"); // these need to be handled separately
            //prefixes.push_back("mask");

            // if this directory name changes from "./", the erase call below may also need to change
            boost::filesystem::path p (".");

            typedef std::vector<boost::filesystem::path> path_vec;
            path_vec v;

            copy(boost::filesystem::directory_iterator(p),
                 boost::filesystem::directory_iterator(), back_inserter(v));

            // find mosaics by looking for images that contain one of the tags.
            // Then see which of those contain all tags.
            const std::string searchTag = imageTags[0];

            for (path_vec::const_iterator it (v.begin()); it != v.end(); ++it) {

                // set name of the current file name and remove "./"
                std::string name = it->string();
                name.erase(0,2);

                // make sure this is a directory
                // a sym link to a directory will pass this test
                if (!boost::filesystem::is_directory(*it)) {
                    //ASKAPLOG_INFO_STR(linmoslogger, name << " is not a directory. Ignoring.");
                    continue;
                }

                // see if the name contains the desired tag (i.e., contains the first tag in "names")
                size_t pos = name.find(searchTag);
                if (pos == std::string::npos) {
                    //ASKAPLOG_INFO_STR(linmoslogger, name << " is not a match. Ignoring.");
                    continue;
                }

                // set some variables for problem sub-strings
                std::string restored_tag = ".restored";
                size_t restored_pos;

                // see if the name contains a desired prefix, and if so, check the other input names and weights
                int full_set = 0, full_wgt_set = 0;
                std::string mosaicName = name, nextName = name, tmpName;
                for (std::vector<std::string>::const_iterator pre (prefixes.begin()); pre != prefixes.end(); ++pre) {
                    if (name.find(*pre) == 0) {

                        // both of these must remain set to 1 for this mosaic to be established
                        full_set = 1;
                        full_wgt_set = 1;

                        // set the output mosaic name
                        mosaicName.replace(pos, searchTag.length(), itsMosaicTag);

                        // file seems good, but check that it is present in all input images
                        for (uint img = 0; img < imageTags.size(); ++img) {

                            // name is initially set for image 0
                            nextName = name;
                            // replace the image 0 tag with the current image's tag
                            if (img > 0) {
                                nextName.replace(pos, searchTag.length(), imageTags[img]);
                                // check that the file exists
                                if (!boost::filesystem::exists(nextName)) {
                                    full_set = -1;
                                    break;
                                }
                            }
                            // add the image to this mosaics inputs
                            itsInImgNameVecs[mosaicName].push_back(nextName);

                            // see if there is an appropriate sensitivity image
                            tmpName = nextName;
                            tmpName.replace(0, (*pre).length(), "sensitivity");
                            // remove any ".restored" sub-string from the weights file name
                            restored_pos = tmpName.find(restored_tag);
                            if (restored_pos != std::string::npos) {
                                tmpName.replace(restored_pos, restored_tag.length(), "");
                            }
                            if (boost::filesystem::exists(tmpName)) {
                                itsInSenNameVecs[mosaicName].push_back(tmpName);
                            }

                            // look for weights image if required (weights are
                            // not needed when combining sensitivity images)
                            if (itsWeightType == FROM_WEIGHT_IMAGES || itsWeightType == COMBINED) {
                                // replace the prefix with "weights"
                                nextName.replace(0, (*pre).length(), "weights");
                                // remove any ".restored" sub-string from the weights file name
                                restored_pos = nextName.find(restored_tag);
                                if (restored_pos != std::string::npos) {
                                    nextName.replace(restored_pos, restored_tag.length(), "");
                                }
                                // check that the file exists
                                if (!boost::filesystem::exists(nextName)) {
                                    full_wgt_set = -1;
                                    break;
                                }
                                // add the file to this mosaics inputs
                                itsInWgtNameVecs[mosaicName].push_back(nextName);
                            }

                        }

                        // set the output weights image name
                        // replace the mosaic prefix with "weights"
                        nextName = mosaicName;
                        nextName.replace(0, (*pre).length(), "weights");
                        // remove any ".restored" sub-string from the weights file name
                        restored_pos = nextName.find(restored_tag);
                        if (restored_pos != std::string::npos) {
                            nextName.replace(restored_pos, restored_tag.length(), "");
                        }
                        itsOutWgtNames[mosaicName] = nextName;

                        itsGenSensitivityImage[mosaicName] = false;
                        if (itsInSenNameVecs.find(mosaicName)!=itsInSenNameVecs.end()) { // if key is found
                            if (itsInImgNameVecs[mosaicName].size()==itsInSenNameVecs[mosaicName].size()) {
                                itsGenSensitivityImage[mosaicName] = true;
                                // set an output sensitivity file name
                                tmpName = mosaicName;
                                tmpName.replace(0, (*pre).length(), "sensitivity");
                                // remove any ".restored" sub-string from the weights file name
                                restored_pos = tmpName.find(restored_tag);
                                if (restored_pos != std::string::npos) {
                                    tmpName.replace(restored_pos, restored_tag.length(), "");
                                }
                                itsOutSenNames[mosaicName] = tmpName;
                            } else {
                                itsInSenNameVecs.erase(mosaicName);
                            }
                        }

                        break; // found the prefix, so leave the loop

                    }
                }

                if (full_set==0) {
                    // this file did not have a relevant prefix, so just move on
                    continue;
                }

                if ((full_set == -1) || ((itsWeightType == FROM_WEIGHT_IMAGES || itsWeightType == COMBINED) && (full_wgt_set == -1))) {
                    // this file did have a relevant prefix, but failed
                    if (full_set == -1) {
                        ASKAPLOG_INFO_STR(linmoslogger, mosaicName << " does not have a full set of input files. Ignoring.");
                    }
                    if ((itsWeightType == FROM_WEIGHT_IMAGES || itsWeightType == COMBINED ) && (full_wgt_set == -1)) {
                        ASKAPLOG_INFO_STR(linmoslogger, mosaicName << " does not have a full set of weights files. Ignoring.");
                    }

                    // if any of these were started for the current failed key, clean up and move on
                    if (itsOutWgtNames.find(mosaicName)!=itsOutWgtNames.end()) {
                        itsOutWgtNames.erase(mosaicName);
                    }
                    if (itsOutSenNames.find(mosaicName)!=itsOutSenNames.end()) {
                        itsOutSenNames.erase(mosaicName);
                    }
                    if (itsInImgNameVecs.find(mosaicName)!=itsInImgNameVecs.end()) {
                        itsInImgNameVecs.erase(mosaicName);
                    }
                    if (itsInWgtNameVecs.find(mosaicName)!=itsInWgtNameVecs.end()) {
                        itsInWgtNameVecs.erase(mosaicName);
                    }
                    if (itsInSenNameVecs.find(mosaicName)!=itsInSenNameVecs.end()) {
                        itsInSenNameVecs.erase(mosaicName);
                    }

                    continue;
                }

                // double check the size of the various maps and vectors. These should have been caught already
                ASKAPCHECK(itsInImgNameVecs.size()==itsOutWgtNames.size(),
                    mosaicName << ": inconsistent name maps.");
                if (itsWeightType == FROM_WEIGHT_IMAGES || itsWeightType == COMBINED ) {
                    ASKAPCHECK(itsInImgNameVecs.size()==itsInWgtNameVecs.size(),
                        mosaicName << ": mosaic search error. Inconsistent name maps.");
                    ASKAPCHECK(itsInImgNameVecs[mosaicName].size()==itsInWgtNameVecs[mosaicName].size(),
                        mosaicName << ": mosaic search error. Inconsistent name vectors.");
                }

                ASKAPLOG_INFO_STR(linmoslogger, mosaicName << " seems complete. Mosaicking.");

                // It is possible that there may be duplicate itsOutWgtNames/itsOutSenNames
                // (e.g. for image.* and residual.*).
                // Check that the input is the same for these duplicates, and then only write once
                // if this is common, we should be avoiding more that just duplicate output.
                std::string mosaicOrig;
                itsOutWgtDuplicates[mosaicName] = false;
                for(std::map<std::string,std::string>::iterator ii=itsOutWgtNames.begin();
                    ii!=itsOutWgtNames.find(mosaicName); ++ii) {
                    if (itsOutWgtNames[mosaicName].compare((*ii).second) == 0) {
                        itsOutWgtDuplicates[mosaicName] = true;
                        mosaicOrig = (*ii).first;
                        break;
                    }
                }

                // if this is a duplicate, just remove it. Can't with weights because we need them unaveraged
                if (itsOutSenNames.find(mosaicName)!=itsOutSenNames.end()) {
                    for(std::map<std::string,std::string>::iterator ii=itsOutSenNames.begin();
                        ii!=itsOutSenNames.find(mosaicName); ++ii) {
                        if (itsOutSenNames[mosaicName].compare((*ii).second) == 0) {
                            ASKAPLOG_INFO_STR(linmoslogger,
                                "  - sensitivity image done in an earlier mosaic. Will not redo here.");
                            itsGenSensitivityImage[mosaicName] = false;
                            itsOutSenNames.erase(mosaicName);
                            itsInSenNameVecs.erase(mosaicName);
                            break;
                        }
                    }
                }
                if (itsOutSenNames.find(mosaicName)!=itsOutSenNames.end()) {
                    ASKAPLOG_INFO_STR(linmoslogger,
                        "  - sensitivity images found. Generating mosaic sens. image.");
                }

            } // it loop (over potential images in this directory)

        } // void LinmosAccumulator<T>::findAndSetMosaics()

        template<typename T>
        bool LinmosAccumulator<T>::outputBufferSetupRequired(void) const {
            return ( itsOutBuffer.shape().nelements() == 0 );
        }

        template<typename T>
        void LinmosAccumulator<T>::setInputParameters(const IPosition& inShape,
                                                      const CoordinateSystem& inCoordSys,
                                                      int n) {
            // set the input coordinate system and shape
            itsInShape = inShape;
            itsInCoordSys = inCoordSys;
            if (itsWeightType == FROM_BP_MODEL || itsWeightType == COMBINED) {
                // set the centre of the beam
                if ( n >= 0 && n < itsCentres.size()) {
                    itsInCentre = itsCentres[n];
                }
                if ( n >= 0 && n < itsPBs.size()) {
                    itsInPB = itsPBs[n];
                } else {
                    // invalid beam number - use first beam
                    itsInPB = itsPBs[0];
                }
            }
        }

        template<typename T>
        void LinmosAccumulator<T>::setOutputParameters(const IPosition& outShape,
                                                       const CoordinateSystem& outCoordSys) {
            itsOutShape = outShape;
            itsOutCoordSys = outCoordSys;
        }

        template<typename T>
        int LinmosAccumulator<T>::findCentralField(const std::vector<IPosition>& inShapeVec,
                                                   const std::vector<CoordinateSystem>& inCoordSysVec) {
            // Method: - average central pixel coordinates of all input images
            //         - find field with ref pos closest to the average
            // TODO: remove duplicates from the input? Or use centre of final blc/trc instead?
            //ASKAPLOG_INFO_STR(linmoslogger, "Finding central field");
            const int nfield = inCoordSysVec.size();
            Vector<MVDirection> centres(nfield);
            Vector<double> pix(2);
            Vector<double> av(3,0.);
            for (int i=0; i<nfield; i++) {
                const int dcPos = inCoordSysVec[i].findCoordinate(Coordinate::DIRECTION,-1);
                // assumed below to be the first two axes (dim and shape setting). Checked this here.
                ASKAPDEBUGASSERT(dcPos == 0);
                const DirectionCoordinate dc = inCoordSysVec[i].directionCoordinate(dcPos);
                //ASKAPLOG_INFO_STR(linmoslogger, "Field "<<i<<" ref : "<<dc.referenceValue()*180./C::pi<< " "<< dc.referencePixel());
                pix(0) = inShapeVec[i][0]/2;
                pix(1) = inShapeVec[i][1]/2;
                centres(i) = dc.toWorld(pix);
                //ASKAPLOG_INFO_STR(linmoslogger, "Field "<<i<<" ctr : "<<centres(i).get()*180./C::pi<< " "<< pix);
                    av += centres(i).getValue();
            }
            av /= double(nfield);
            MVDirection avDir(av);
            //ASKAPLOG_INFO_STR(linmoslogger, "Average Field centre "<<avDir.get()*180./C::pi);
            double sep = 1000.;
            int closest = 0;
            for (int i=0; i<nfield; i++) {
                double s = centres(i).separation(avDir);
                //ASKAPLOG_INFO_STR(linmoslogger, "Field "<<i<<" distance to average: "<<s*180/C::pi<<" deg");
                if (s < sep) {
                    closest = i;
                    sep = s;
                }
            }
            ASKAPLOG_INFO_STR(linmoslogger, "Using central field "<< closest << " as the output coordinate reference");
            return closest;
        }

        template<typename T>
        void LinmosAccumulator<T>::setOutputParameters(const std::vector<IPosition>& inShapeVec,
                                                       const std::vector<CoordinateSystem>& inCoordSysVec) {

            ASKAPLOG_INFO_STR(linmoslogger, "Determining output image based on the overlap of input images");
            ASKAPCHECK(inShapeVec.size()==inCoordSysVec.size(), "Input vectors are inconsistent");
            ASKAPCHECK(inShapeVec.size()>0, "Number of input vectors should be greater than 0");

            int ref = (itsOutputCentreDefined ? 0 : itsOutputRef);
            if (ref<0) {
                itsOutputRef = findCentralField(inShapeVec,inCoordSysVec);
                ref = itsOutputRef;
            }

            IPosition refShape = inShapeVec[ref];
            CoordinateSystem refCS = inCoordSysVec[ref];
            ASKAPDEBUGASSERT(refShape.nelements() >= 2);
            const int dcPos = refCS.findCoordinate(Coordinate::DIRECTION,-1);
            // assumed below to be the first two axes (dim and shape setting). Checked this here.
            ASKAPDEBUGASSERT(dcPos == 0);
            MDirection::Types inputDirType = refCS.directionCoordinate(dcPos).directionType();
            Projection inputProjection = refCS.directionCoordinate(dcPos).projection();
            DirectionCoordinate refDC;

            if (itsOutputCentreDefined) {
                Vector<Double> refVal(itsOutputCentre.getAngle().get().getValue());
                if (refVal(0)<0) {
                  refVal(0) += 2*C::pi;
                }
                Vector<Double> inc = refCS.directionCoordinate(dcPos).increment();
                Vector<Double> refPix(2,0.0);
                Matrix<Double> xform(2,2,0.0);
                xform.diagonal() = 1.0;
                refDC = DirectionCoordinate(MDirection::castType(itsOutputCentre.getRef().getType()),
                  Projection(itsOutputProjectionType),refVal(0),refVal(1),inc(0),inc(1),xform,refPix(0),refPix(1));
                refShape(0) = 1;
                refShape(1) = 1;
            } else {
                // set output dir type and projection if needed
                if (itsOutputDirType == MDirection::N_Types) {
                    itsOutputDirType = inputDirType;
                }
                if (itsOutputProjectionType == Projection::N_PROJ) {
                    itsOutputProjectionType = inputProjection.type();
                }

                if (itsOutputDirType != inputDirType || itsOutputProjectionType != inputProjection.type()){
                  Vector<Double> refVal = refCS.directionCoordinate(dcPos).referenceValue();
                  Vector<Double> refPix = refCS.directionCoordinate(dcPos).referencePixel();
                  Vector<Double> inc = refCS.directionCoordinate(dcPos).increment();
                  Matrix<Double> xform(2,2,0.0);
                  xform.diagonal() = 1.0;
                  if (itsOutputDirType != inputDirType) {
                    // convert output centre to new Direction type
                    MDirection newRef = MDirection::Convert(MDirection(Quantity(refVal(0), "rad"),Quantity(refVal(1), "rad"),
                                                            MDirection::Ref(inputDirType)),itsOutputDirType)();
                    refVal = newRef.getAngle().get().getValue();
                  }
                  refDC = DirectionCoordinate(itsOutputDirType, Projection(itsOutputProjectionType),refVal(0),refVal(1),
                                              inc(0),inc(1),xform,refPix(0),refPix(1));
                  refShape(0) = 1;
                  refShape(1) = 1;

                } else {
                    refDC = refCS.directionCoordinate(dcPos);
                }
            }

            IPosition refBLC(refShape.nelements(),0);
            IPosition refTRC(refShape-1);
            ASKAPDEBUGASSERT(refBLC.nelements() >= 2);
            ASKAPDEBUGASSERT(refTRC.nelements() >= 2);

            IPosition tempBLC(refShape.nelements());
            IPosition tempTRC(refShape.nelements());
            // Loop over input vectors, converting their image bounds to the ref system
            // and expanding the new overlapping image bounds where appropriate.
            itsRegridOutputBufferMap.clear();
            for (uint img = 0; img < inShapeVec.size(); ++img ) {

                itsInShape = inShapeVec[img];
                itsInCoordSys = inCoordSysVec[img];

                // test to see if the loaded coordinate system is close enough to the reference system for merging
                ASKAPCHECK(coordinatesAreConsistent(itsInCoordSys, refCS),
                    "Input images have inconsistent coordinate systems");
                // could also test whether they are equal and set a regrid tag to false if all of them are

                const int coordPos = itsInCoordSys.findCoordinate(Coordinate::DIRECTION,-1);
                const DirectionCoordinate inDC = itsInCoordSys.directionCoordinate(coordPos);

                // need to check all four corners, because of the curved coordinates
                // actually may need to check entire edge, but adding midpoints for now
                Vector<IPosition> edgePoints = convertImageEdgePointsToRef(inDC, itsInShape,
                     refDC, false, true);

                // corresponding output image blc/trc of the input image
                IPosition outputImgBLC(2,0);
                IPosition outputImgTRC(2,0);

                if (img==0){
                    ASKAPCHECK(!edgePoints.empty(),"edge points vector is empty");
                    tempBLC(0) = edgePoints[0](0);
                    tempTRC(0) = edgePoints[0](0);
                    tempBLC(1) = edgePoints[0](1);
                    tempTRC(1) = edgePoints[0](1);
                }

                outputImgBLC(0) = edgePoints[0](0);
                outputImgTRC(0) = edgePoints[0](0);
                outputImgBLC(1) = edgePoints[0](1);
                outputImgTRC(1) = edgePoints[0](1);

                for (const IPosition& point : edgePoints) {
                    ASKAPDEBUGASSERT(point.nelements() >=2);
                    tempBLC(0) = std::min(tempBLC(0),point(0));
                    tempTRC(0) = std::max(tempTRC(0),point(0));
                    tempBLC(1) = std::min(tempBLC(1),point(1));
                    tempTRC(1) = std::max(tempTRC(1),point(1));

                    // For each input image, determine the correspondng output blc/trc
                    outputImgBLC(0) = std::min(outputImgBLC(0),point(0));
                    outputImgTRC(0) = std::max(outputImgTRC(0),point(0));
                    outputImgBLC(1) = std::min(outputImgBLC(1),point(1));
                    outputImgTRC(1) = std::max(outputImgTRC(1),point(1));
                }

                // Store the corresponding output blc/trc of each input image
                // in a map which will be used in the regrid2() method
                BoundingBoxT p = std::make_pair(outputImgBLC,outputImgTRC);
                itsRegridOutputBufferMap.insert(std::make_pair(img,p));
            }

            // adjust/shift the blc/trc to the blc of the overall output image
            // ie the tempBLC
            for (uint img = 0; img < inShapeVec.size(); ++img ) {
                BoundingBoxT& BlcTrcPair = itsRegridOutputBufferMap[img];
                IPosition& blc = BlcTrcPair.first;
                IPosition& trc = BlcTrcPair.second;
                blc(0) -= tempBLC(0);
                blc(1) -= tempBLC(1);
                trc(0) -= tempBLC(0);
                trc(1) -= tempBLC(1);
            }


            for (const auto& kvp : itsRegridOutputBufferMap) {
                ASKAPLOG_DEBUG_STR(linmoslogger, "Output BLC/TRC of input image " << kvp.first
                                        << ", blc - " << kvp.second.first
                                        << ", trc - " << kvp.second.second);
            }

            itsOutShape = refShape;
            itsOutShape(0) = tempTRC(0) - tempBLC(0) + 1;
            itsOutShape(1) = tempTRC(1) - tempBLC(1) + 1;
            ASKAPDEBUGASSERT(itsOutShape(0) > 0);
            ASKAPDEBUGASSERT(itsOutShape(1) > 0);
            Vector<Double> refPix = refDC.referencePixel();
            refPix[0] -= Double(tempBLC(0) - refBLC(0));
            refPix[1] -= Double(tempBLC(1) - refBLC(1));
            DirectionCoordinate newDC(refDC);
            newDC.setReferencePixel(refPix);

            ASKAPLOG_INFO_STR(linmoslogger,"Output shape: "<< itsOutShape<< " Ref pixel: "<<refPix);

            // set up a coord system for the merged images
            itsOutCoordSys = refCS;
            itsOutCoordSys.replaceCoordinate(newDC, dcPos);
        }

        template<typename T>
        void LinmosAccumulator<T>::adjustOutputImageMap(const IPosition& outputBLC, const IPosition& outputShape)
        {
            for (auto& kvp : itsRegridOutputBufferMap) {
                IPosition& blc = kvp.second.first;
                IPosition& trc = kvp.second.second;

                // adjust the current blc to the new outputBLC
                blc(0) -= outputBLC(0);
                blc(1) -= outputBLC(1);
                trc(0) -= outputBLC(0);
                trc(1) -= outputBLC(1);

                ASKAPLOG_DEBUG_STR(linmoslogger, "Before New Output BLC/TRC of input image "
                                        << ", blc - " << blc
                                        << ", trc - " << trc
                                        << ", Output Shape - " << outputShape);

                // clip the blc if it is smaller 0
                if ( blc(0) < 0 ) {
                    blc(0) = 0;
                }

                if ( blc(1) < 0 ) {
                    blc = 0;
                }

                // clip the trc if it is greater the shape of the output image
                if ( trc(0)  >  outputShape(0) - 1) {
                    trc(0) = outputShape(0) - 1;
                }

                if ( trc(1) > outputShape(1) - 1) {
                    trc(1) = outputShape(1) - 1;
                }

                ASKAPLOG_DEBUG_STR(linmoslogger, "After New Output BLC/TRC of input image "
                                        << ", blc - " << blc
                                        << ", trc - " << trc
                                        << ", used shaped - " << trc - blc - 1
                                        << ", Output Shape - " << outputShape);
                ASKAPDEBUGASSERT(blc(0) <= trc(0));
                ASKAPDEBUGASSERT(blc(1) <= trc(1));
            }
        }

        template<typename T>
        void LinmosAccumulator<T>::initialiseOutputBuffers(void) {
            // set up temporary images needed for regridding (which is
            // done on a plane-by-plane basis so ignore other dims)

            // set up the coord. sys.
            int dcPos = itsOutCoordSys.findCoordinate(Coordinate::DIRECTION,-1);
            ASKAPCHECK(dcPos>=0, "Cannot find the directionCoordinate");
            const DirectionCoordinate dcTmp = itsOutCoordSys.directionCoordinate(dcPos);
            CoordinateSystem cSysTmp;
            cSysTmp.addCoordinate(dcTmp);

            // set up the shape
            Vector<Int> shapePos = itsOutCoordSys.pixelAxes(dcPos);
            // check that the length is equal to 2 and the both elements are >= 0
            ASKAPCHECK(shapePos.nelements()>=2, "Cannot find the directionCoordinate");
            ASKAPCHECK((shapePos[0]==0 && shapePos[1]==1) || (shapePos[1]==0 && shapePos[0]==1),
                       "Linmos currently requires the direction coordinates to come before any others");

            IPosition shape = IPosition(2,itsOutShape(shapePos[0]),itsOutShape(shapePos[1]));

            // apparently the +100 forces it to use the memory
            double maxMemoryInMB = double(shape.product()*sizeof(T))/1024./1024.+100;
            itsOutBuffer = TempImage<T>(shape, cSysTmp, maxMemoryInMB);
            ASKAPCHECK(itsOutBuffer.shape().nelements()>0, "Output buffer does not appear to be set");

            itsOutWgtBuffer = TempImage<T>(shape, cSysTmp, maxMemoryInMB);
            ASKAPCHECK(itsOutWgtBuffer.shape().nelements()>0, "Output weights buffer does not appear to be set");

            if (itsDoSensitivity) {
                itsOutSnrBuffer = TempImage<T>(shape, cSysTmp, maxMemoryInMB);
                ASKAPCHECK(itsOutSnrBuffer.shape().nelements()>0,
                    "Output sensitivity buffer does not appear to be set");
            }
        }

        template<typename T>
        void LinmosAccumulator<T>::redirectOutputBuffers(void) {
            // if not regridding point output buffers at input buffers
            // TempImage operator= uses reference semantic...

            itsOutBuffer = itsInBuffer;
            ASKAPCHECK(itsOutBuffer.shape().nelements()>0, "Output buffer does not appear to be set");

            itsOutWgtBuffer = itsInWgtBuffer;
            ASKAPCHECK(itsOutWgtBuffer.shape().nelements()>0, "Output weights buffer does not appear to be set");

            if (itsDoSensitivity) {
                itsOutSnrBuffer = itsInSnrBuffer;
                ASKAPCHECK(itsOutSnrBuffer.shape().nelements()>0,
                    "Output sensitivity buffer does not appear to be set");
            }
        }

        template<typename T>
        void LinmosAccumulator<T>::initialiseInputBuffers() {
            // set up temporary images needed for regridding (which is
            // done on a plane-by-plane basis so ignore other dims)

            // set up a coord. sys. the planes
            int dcPos = itsInCoordSys.findCoordinate(Coordinate::DIRECTION,-1);
            ASKAPCHECK(dcPos>=0, "Cannot find the directionCoordinate");
            const DirectionCoordinate dc = itsInCoordSys.directionCoordinate(dcPos);
            CoordinateSystem cSys;
            cSys.addCoordinate(dc);

            // set up the shape
            Vector<Int> shapePos = itsInCoordSys.pixelAxes(dcPos);
            // check that the length is equal to 2 and the both elements are >= 0

            IPosition shape = IPosition(2,itsInShape(shapePos[0]),itsInShape(shapePos[1]));

            double maxMemoryInMB = double(shape.product()*sizeof(T))/1024./1024.+100;
            itsInBuffer = TempImage<T>(shape,cSys,maxMemoryInMB);
            ASKAPCHECK(itsInBuffer.shape().nelements()>0, "Input buffer does not appear to be set");

            itsInWgtBuffer = TempImage<T>(shape,cSys,maxMemoryInMB);
            ASKAPCHECK(itsInWgtBuffer.shape().nelements()>0, "Input weights buffer does not appear to be set");

            if (itsDoSensitivity) {
                itsInSenBuffer = TempImage<T>(shape,cSys,maxMemoryInMB);
                itsInSnrBuffer = TempImage<T>(shape,cSys,maxMemoryInMB);
                ASKAPCHECK(itsInSnrBuffer.shape().nelements()>0,
                    "Input sensitivity buffer does not appear to be set");
            }

        }

        template<typename T>
        void LinmosAccumulator<T>::initialiseRegridder() {
            ASKAPLOG_INFO_STR(linmoslogger, "Initialising regridder for " << itsMethod << " interpolation");
            itsAxes = IPosition::makeAxisPath(itsOutBuffer.shape().nelements());
            itsEmethod = Interpolate2D::stringToMethod(itsMethod);
        }

        template<typename T>
        void LinmosAccumulator<T>::loadAndWeightInputBuffers(const IPosition& curpos,
                                                             Array<T>& inPix,
                                                             Array<T>& inWgtPix,
                                                             Array<T>& inSenPix) {
            //double timerStart = MPI_Wtime();
            if (itsWeightType == FROM_WEIGHT_IMAGES) {
                ASKAPLOG_INFO_STR(linmoslogger,"weighttype = FromWeightImages");
            }
            if (itsWeightType == COMBINED) {
                ASKAPLOG_INFO_STR(linmoslogger,"weighttype = Combined");
            }
            if (itsWeightType == FROM_BP_MODEL) {
                ASKAPLOG_INFO_STR(linmoslogger,"weighttype = FromPrimaryBeamModel");
            }
            if (itsWeightState == CORRECTED) {
                ASKAPLOG_INFO_STR(linmoslogger,"weightstate = Corrected");
            }
            if (itsWeightState == INHERENT) {
                ASKAPLOG_INFO_STR(linmoslogger,"weightstate = Inherent");
            }
            if (itsWeightState == WEIGHTED) {
                ASKAPLOG_INFO_STR(linmoslogger,"weightstate = Weighted");
            }
            // could extract the plane without an iterator, but will use one for consistency
            const imagemath::MultiDimArrayPlaneIter planeIter(inPix.shape());
            const int nx = inPix.shape()[0];
            const int ny = inPix.shape()[1];
            const IPosition shape2d(2, nx, ny);

            // CORRECTED: img
            // INHERENT:  img * pb
            // WEIGHTED:  img * pb^2

            itsInBuffer.put(planeIter.getPlane(inPix, curpos).reform(shape2d));

            if (itsWeightType == FROM_WEIGHT_IMAGES || itsWeightType == COMBINED ) {
                // FROM_WEIGHT_IMAGES: sum(invvar) * pb^2
                // COMBINED:           sum(invvar)
                itsInWgtBuffer.put(planeIter.getPlane(inWgtPix, curpos).reform(shape2d));
            }

            if (itsWeightType == FROM_BP_MODEL || itsWeightType == COMBINED) {

                const int scPos = itsInCoordSys.findCoordinate(Coordinate::SPECTRAL,-1);
                const SpectralCoordinate inSC = itsInCoordSys.spectralCoordinate(scPos);
                int chPos = itsInCoordSys.pixelAxes(scPos)[0];
                const double freq = inSC.referenceValue()[0] +
                               (curpos[chPos] - inSC.referencePixel()[0]) * inSC.increment()[0];

                const int dcPos = itsInCoordSys.findCoordinate(Coordinate::DIRECTION,-1);
                const DirectionCoordinate inDC = itsInCoordSys.directionCoordinate(dcPos);

                Vector<Double> ref(2);
                inDC.toPixel(ref,itsInCentre);

                const double incx = inDC.increment()[0];
                const double incy = inDC.increment()[1];

                #pragma omp parallel
                {
                    IPosition pos(2);
                    #pragma omp for
                    for (int y=0; y<ny; ++y) {
                        for (int x=0; x<nx; ++x) {
                            pos[0] = x;
                            pos[1] = y;
                            double offsetx = incx * double(x-ref[0]);
                            double offsety = incy * double(y-ref[1]);
                            // this seems to be giving the same as world0.separation(world1)
                            double offsetBeam = asin( sqrt( offsetx*offsetx + offsety*offsety ) );
                            // this seems to be giving the same as world0.positionAngle(world1)
                            double offsetAngle = atan2(offsetx,offsety);
                            // set the weight
                            //pb = exp(-offsetBeam*offsetBeam*4.*log(2.)/fwhm/fwhm);
                            const T pb = itsInPB->evaluateAtOffset(offsetAngle,offsetBeam,freq);
                            if (itsWeightType == FROM_BP_MODEL) {
                                if (itsWeightState == CORRECTED) {
                                    itsInBuffer.putAt( itsInBuffer.getAt(pos) * pb * pb, pos );
                                }
                                else if (itsWeightState == INHERENT) {
                                    itsInBuffer.putAt( itsInBuffer.getAt(pos) * pb, pos );
                                }
                                // else itsWeightState == WEIGHTED -- nothing to do
                                itsInWgtBuffer.putAt( pb * pb, pos );
                            }
                            else if (itsWeightType == COMBINED) {
                                const T wt = itsInWgtBuffer.getAt(pos);
                                if (itsWeightState == CORRECTED) {
                                    itsInBuffer.putAt( itsInBuffer.getAt(pos) * wt * pb * pb, pos );
                                }
                                else if (itsWeightState == INHERENT) {
                                    itsInBuffer.putAt( itsInBuffer.getAt(pos) * wt * pb, pos );
                                }
                                else { // WEIGHTED
                                    itsInBuffer.putAt( itsInBuffer.getAt(pos) * wt, pos );
                                }
                                itsInWgtBuffer.putAt( wt * pb * pb, pos );
                            }
                            // else if (itsWeightType == FROM_WEIGHT_IMAGES) // done separately below
                        }
                    }
                }
            }
            else { // FROM_WEIGHT_IMAGES
                #pragma omp parallel
                {
                    IPosition pos(2);
                    #pragma omp for
                    for (int y=0; y<ny; ++y) {
                        for (int x=0; x<nx; ++x) {
                            pos[0] = x;
                            pos[1] = y;
                            if (itsWeightState == CORRECTED) {
                                itsInBuffer.putAt( itsInBuffer.getAt(pos) * itsInWgtBuffer.getAt(pos), pos );
                            }
                            else if (itsWeightState == INHERENT) {
                                // Need wgt=sqrt(pb^2), but pb^2 may be multiplied with sum(invvar). Two options:
                                // Assume max(pb)=1 so that max(itsInWgtBuffer) = sum(invvar)
                                //     T minVal, maxVal, wgtCutoff;
                                //     IPosition minPos, maxPos;
                                //     minMax(minVal,maxVal,minPos,maxPos,itsInWgtBuffer);
                                //     T invvar = maxVal;
                                //     itsInWgtBuffer /= invvar;
                                // OR
                                // Assume itsInWgtBuffer=pb^2 and ignore sum(invvar)
                                itsInBuffer.putAt( itsInBuffer.getAt(pos) * sqrt(itsInWgtBuffer.getAt(pos)), pos );
                            }
                            // else itsWeightState == WEIGHTED -- nothing to do
                        }
                    }
                }
            }

            if (itsDoSensitivity) {
                // invert sensitivities before regridding to avoid
                // artefacts at sharp edges in the sensitivity image
                itsInSenBuffer.put(planeIter.getPlane(inSenPix, curpos).reform(shape2d));
                #pragma omp parallel
                {
                    IPosition pos(2);
                    #pragma omp for
                    for (int y=0; y<inSenPix.shape()[1];++y) {
                        for (int x=0; x<inSenPix.shape()[0];++x) {
                            pos[0] = x;
                            pos[1] = y;
                            T sensitivity = itsInSenBuffer.getAt(pos);
                            if (sensitivity>0) {
                                itsInSnrBuffer.putAt(1.0 / (sensitivity * sensitivity), pos);
                            } else {
                                itsInSnrBuffer.putAt(0.0, pos);
                            }
                        }
                    }
                }
            }
            //double timerStop = MPI_Wtime();
            //ASKAPLOG_INFO_STR(linmoslogger,"loadAndWeightInputBuffers took "<< timerStop-timerStart<<"s");
        }

        template<typename T>
        void LinmosAccumulator<T>::regrid()
        {
            //double timerStart = MPI_Wtime();
            //double timerStop1;
            ASKAPLOG_INFO_STR(linmoslogger, "regridding with dec="<<
                    itsDecimate<<" rep="<<itsReplicate<<" force="<<itsForce);
            ASKAPCHECK(itsOutBuffer.shape().nelements()>0,
                    "Output buffer does not appear to be set");
            //itsRegridder.showDebugInfo(1);
            itsRegridder.regrid(itsOutBuffer, itsEmethod, itsAxes, itsInBuffer, itsReplicate, itsDecimate, itsForce);
            //timerStop1 = MPI_Wtime();
            // reuse the coordinate grid for the next two calls
            itsRegridder.reuse2DCoordinateGrid(True);
            itsRegridder.regrid(itsOutWgtBuffer, itsEmethod, itsAxes, itsInWgtBuffer, itsReplicate, itsDecimate, itsForce);
            if (itsDoSensitivity) {
                itsRegridder.regrid(itsOutSnrBuffer, itsEmethod, itsAxes, itsInSnrBuffer, itsReplicate, itsDecimate, itsForce);
            }
            // invalidate the coordinate grid, ready for next beam
            itsRegridder.reuse2DCoordinateGrid(False);
            //double timerStop = MPI_Wtime();
            //ASKAPLOG_INFO_STR(linmoslogger," first regrid took "<< timerStop1-timerStart<<"s");
            //ASKAPLOG_INFO_STR(linmoslogger," 2nd  regrid took "<< timerStop-timerStop1<<"s");
            //ASKAPLOG_INFO_STR(linmoslogger,"regrid took "<< timerStop-timerStart<<"s");
        }

        template<typename T>
        void LinmosAccumulator<T>::regrid(uint img)
        {
            //double timerStart = MPI_Wtime();
            //double timerStop1;
            ASKAPLOG_INFO_STR(linmoslogger, "regridding with dec="<<
                    itsDecimate<<" rep="<<itsReplicate<<" force="<<itsForce);
            ASKAPCHECK(itsOutBuffer.shape().nelements()>0,
                    "Output buffer does not appear to be set");
            // set up a coord. sys. the planes
            const BoundingBoxT& BlcTrcPair = itsRegridOutputBufferMap[img];
            IPosition outputShape = itsOutBuffer.shape();
            IPosition blc = BlcTrcPair.first;
            IPosition trc = BlcTrcPair.second;

            ASKAPLOG_DEBUG_STR(linmoslogger,"---> img " << img << ", output: blc " << BlcTrcPair.first
                                           << ", trc " << BlcTrcPair.second
                                           << ", blc used: " << blc
                                           << ", trc used: " << trc
                                           << ", Used Output Buffer Shape" << trc - blc - 1
                                           << ", Output Buffer Shape: " << itsOutBuffer.shape());

            SubImage<T> subImageBuffer = SubImage<T>(itsOutBuffer,Slicer(blc,trc,casacore::Slicer::endIsLast),true);
            itsRegridder.regrid(subImageBuffer, itsEmethod, itsAxes, itsInBuffer, itsReplicate, itsDecimate, itsForce);

            itsRegridder.reuse2DCoordinateGrid(True);
            subImageBuffer = SubImage<T>(itsOutWgtBuffer,Slicer(blc,trc,casacore::Slicer::endIsLast),true);
            itsRegridder.regrid(subImageBuffer, itsEmethod, itsAxes, itsInWgtBuffer, itsReplicate, itsDecimate, itsForce);
            if ( itsDoSensitivity) {
                subImageBuffer = SubImage<T>(itsOutSnrBuffer,Slicer(blc,trc,casacore::Slicer::endIsLast),true);
                itsRegridder.regrid(subImageBuffer, itsEmethod, itsAxes, itsInSnrBuffer, itsReplicate, itsDecimate, itsForce);
            }
            // invalidate the coordinate grid, ready for next beam
            itsRegridder.reuse2DCoordinateGrid(False);
            //double timerStop = MPI_Wtime();
            //ASKAPLOG_INFO_STR(linmoslogger," first regrid took "<< timerStop1-timerStart<<"s");
            //ASKAPLOG_INFO_STR(linmoslogger," 2nd  regrid took "<< timerStop-timerStop1<<"s");
            //ASKAPLOG_INFO_STR(linmoslogger,"regrid took "<< timerStop-timerStart<<"s");
        }

        template<typename T>
        void LinmosAccumulator<T>::accumulatePlane( Array<T>& outPix,
                                                    Array<T>& outWgtPix,
                                                    Array<T>& outSenPix,
                                                    const IPosition& curpos,
                                                    const int img) {

            //double timerStart = MPI_Wtime();
            T wgtCutoff = 0;
            T snrCutoff = 0;
            #pragma omp parallel
            {
                // I really worry about the replication here - there must be away
                // to avoid this.

                // copy the pixel iterator containing all dimensions
                //IPosition fullpos(curpos);
                // set a pixel iterator that does not have the higher dimensions
                IPosition pos(2);

                if (itsWeightType == FROM_WEIGHT_IMAGES) {
                    T maxVal = 0.0;
                    #pragma omp master
                    ASKAPLOG_INFO_STR(linmoslogger,
                        "From Weight Images beam weighting do not implement cutoff - maxVal: " << maxVal);
                } else {
                    #pragma omp single
                    {
                        //double timerStart1 = MPI_Wtime();
                        T minVal, maxVal;
                        IPosition minPos, maxPos;
                        minMax(minVal,maxVal,minPos,maxPos,itsOutWgtBuffer);
                        ASKAPLOG_INFO_STR(linmoslogger, "Primary beam weighting - maxVal: " << maxVal );
                        wgtCutoff = itsCutoff * itsCutoff * maxVal; // itsOutWgtBuffer is prop. to image (gain/sigma)^2
                        //double timerStop1 = MPI_Wtime();
                        //ASKAPLOG_INFO_STR(linmoslogger,"accumulatePlane minMax took "<< timerStop1-timerStart1<<"s");
                    }
                }
                #pragma omp master
                ASKAPLOG_INFO_STR(linmoslogger,"Weight cut-off: " << wgtCutoff);

                // Accumulate the pixels of this slice.
                // Could restrict it (and the regrid) to a smaller region of interest.
                int nx = outPix.shape()[0];
                int ny = outPix.shape()[1];

                IPosition blc(2,0);
                IPosition trc(2,nx,ny);
                if ( img >= 0  ) {
                    uint idx = static_cast<uint> (img);
                    const auto& iter =  itsRegridOutputBufferMap.find(idx);
                    if ( iter != itsRegridOutputBufferMap.end() ) {
                        const std::pair<IPosition,IPosition>& blcTrcPair = iter->second;
                        blc = blcTrcPair.first;
                        trc = blcTrcPair.second;
                    }
                }

                // Assuming contiguous arrays
                T* pOutPix = &outPix(curpos);
                T* pOutWgtPix = &outWgtPix(curpos);
                #pragma omp for
                for (int y=blc(1); y<trc(1); ++y) {
                    size_t i = y * nx + blc(0);
                    for (int x=blc(0); x<trc(0); ++x, i++) {
                        pos[0] = x;
                        pos[1] = y;
                        T wt = itsOutWgtBuffer.getAt(pos);
                        T val = itsOutBuffer.getAt(pos);
                        if (wt >= wgtCutoff && !std::isnan(wt) && !std::isnan(val)) {
                            pOutPix[i] += val;
                            pOutWgtPix[i] += wt;
                        }
                    }
                }

                // Accumulate sensitivity for this slice.
                if (itsDoSensitivity) {
                    #pragma omp single
                    {
                        T minVal, maxVal;
                        IPosition minPos, maxPos;
                        minMax(minVal,maxVal,minPos,maxPos,itsOutSnrBuffer);
                        snrCutoff = itsCutoff * itsCutoff * maxVal;
                    }
                    T* pOutSenPix = &outSenPix(curpos);
                    #pragma omp for
                    for (int y=blc(1); y<trc(1); ++y) {
                        size_t i = y * nx + blc(0);
                        for (int x=blc(0); x<trc(0); ++x, i++) {
                            pos[0] = x;
                            pos[1] = y;
                            T wt = itsOutWgtBuffer.getAt(pos);
                            T invVariance = itsOutSnrBuffer.getAt(pos);
                            if (invVariance>=snrCutoff && wt >=wgtCutoff &&
                                !std::isnan(wt) && !std::isnan(invVariance)) {
                                    pOutSenPix[i] += invVariance;
                            }
                        }
                    }
                }
            } // omp parallel
            //double timerStop = MPI_Wtime();
            //ASKAPLOG_INFO_STR(linmoslogger,"accumulatePlane took "<< timerStop-timerStart<<"s");
        }

        template<typename T>
        void LinmosAccumulator<T>::deweightPlane(Array<T>& outPix,
                                                 const Array<T>& outWgtPix,
                                                 Array<T>& outSenPix,
                                                 const IPosition& curpos) {

            //double timerStart = MPI_Wtime();
            #pragma omp parallel
            {
                int nx = outPix.shape()[0];
                int ny = outPix.shape()[1];
                // Assuming contiguous arrays
                T* pOutPix = &outPix(curpos);
                const T* pOutWgtPix = &outWgtPix(curpos);
                #pragma omp for
                for (int y=0; y<ny; ++y) {
                    size_t i = y * nx;
                    for (int x=0; x<nx; ++x, i++) {
                        if (isNaN(pOutWgtPix[i])) {
                            setNaN(pOutPix[i]);
                        } else if (pOutWgtPix[i]>0.0) {
                            //outPix(fullpos) = outPix(fullpos) * normalizer / outWgtPix(fullpos);
                            pOutPix[i] /= pOutWgtPix[i];
                        } else {
                            // should we set outPix and outWgtPix to NaN?
                            pOutPix[i] = 0.0;
                        }
                    }
                }

                if (itsDoSensitivity) {
                    T* pOutSenPix = &outSenPix(curpos);
                    #pragma omp for
                    for (int y=0; y<ny; ++y) {
                        size_t i = y * nx;
                        for (int x=0; x<nx; ++x, i++) {
                            if (isNaN(pOutWgtPix[i])) {
                                setNaN(pOutSenPix[i]);
                            } else if (pOutSenPix[i]>0.0) {
                                pOutSenPix[i] = sqrt(1.0 / pOutSenPix[i]);
                            } else {
                                pOutSenPix[i] = 0.0;
                            }
                        }
                    }
                }
            } // omp parallel
            //double timerStop = MPI_Wtime();
            //ASKAPLOG_INFO_STR(linmoslogger,"deweightPlane took "<< timerStop-timerStart<<"s");
        }
        template<typename T>
        void LinmosAccumulator<T>::weightPlane(Array<T>& outPix,
                                               const Array<T>& outWgtPix,
                                               Array<T>& outSenPix,
                                               const IPosition& curpos) {

            //double timerStart = MPI_Wtime();
            #pragma omp parallel
            {
                int nx = outPix.shape()[0];
                int ny = outPix.shape()[1];
                // Assuming contiguous arrays
                T* pOutPix = &outPix(curpos);
                const T* pOutWgtPix = &outWgtPix(curpos);
                #pragma omp for
                for (int y=0; y<ny; ++y) {
                    size_t i = y * nx;
                    for (int x=0; x<nx; ++x, i++) {
                        if (isNaN(pOutWgtPix[i])) {
                            setNaN(pOutPix[i]);
                        } else if (pOutWgtPix[i]>0.0) {
                            pOutPix[i] *= pOutWgtPix[i];
                        } else {
                            pOutPix[i] = 0.0;
                        }
                    }
                }

                if (itsDoSensitivity) {
                    T* pOutSenPix = &outSenPix(curpos);
                    #pragma omp for
                    for (int y=0; y<ny; ++y) {
                        size_t i = y * nx;
                        for (int x=0; x<nx; ++x, i++) {
                            if (isNaN(pOutWgtPix[i])) {
                                setNaN(pOutSenPix[i]);
                            }
                            else if (pOutSenPix[i]>0.0) {
                                // this is just the reverse of the deWeight operation
                                pOutSenPix[i] *= pOutSenPix[i];
                            } else {
                                pOutSenPix[i] = 0.0;
                            }
                        }
                    }
                }
            } // omp parallel
            //double timerStop = MPI_Wtime();
            //ASKAPLOG_INFO_STR(linmoslogger,"weightPlane took "<< timerStop-timerStart<<"s");
        }

        /// @brief convert the  input shape and coordinate system to the reference (output) system
        /// @param[in] const DirectionCoordinate& inDC: input direction coordinate
        /// @param[in] const IPosition& inShape: input shape
        /// @param[in] const DirectionCoordinate& refDC: reference direction coordinate
        /// @param[in] bool fullEdge : calculate for the full image edge
        /// @param[in] bool midEdge : calculate corners and midpoints of image edge
        /// @param[out] float* x : if not zero, filled with x coordinates (fractional pixels)
        /// @param[out] float* y : if not zero, filled with y coordinates (fractional pixels)
        /// @return IPosition vector containing edge points of the input image,
        ///         relative to another coord. system
        template<typename T>
        Vector<IPosition> LinmosAccumulator<T>::convertImageEdgePointsToRef(const DirectionCoordinate& inDC,
            const IPosition& inShape, const DirectionCoordinate& refDC, bool fullEdge, bool midEdge,
            Vector<float>* x, Vector<Float>* y)
        {
            // based on SynthesisParamsHelper::facetSlicer, but don't want
            // to load every input image into a scimath::Param

            ASKAPDEBUGASSERT(inShape.nelements() >= 2);
            // add more checks

            // need to check at least corners and edge midpoints,
            // because of the curved coordinates
            int nDim = inShape.nelements();
            int nPoints = (fullEdge ? 2*(inShape[0]+inShape[1]-2) :
              (midEdge ? 8 : 4));
            Vector<IPosition> inEdgePoints(nPoints,IPosition(nDim,0));
            Vector<IPosition> outEdgePoints(nPoints,IPosition(nDim,0));
            if (!fullEdge && !midEdge) {
              inEdgePoints(1)[0] = inShape[0] - 1; // BRC
              inEdgePoints(2)[0] = inShape[0] - 1; // TRC
              inEdgePoints(2)[1] = inShape[1] - 1; // TRC
              inEdgePoints(3)[1] = inShape[1] - 1; // TLC
            }
            // keep the points in polygon order
            if (!fullEdge && midEdge) {
              inEdgePoints(1)[0] = inShape[0]/2;   // Bottom Midpoint
              inEdgePoints(2)[0] = inShape[0] - 1; // BRC
              inEdgePoints(3)[0] = inShape[0] - 1; // Right Midpoint
              inEdgePoints(3)[1] = inShape[1]/2;   // Right Midpoint
              inEdgePoints(4)[0] = inShape[0] - 1; // TRC
              inEdgePoints(4)[1] = inShape[1] - 1; // TRC
              inEdgePoints(5)[0] = inShape[0]/2;   // Top Midpoint
              inEdgePoints(5)[1] = inShape[1] - 1; // Top Midpoint
              inEdgePoints(6)[1] = inShape[1] - 1; // TLC
              inEdgePoints(7)[1] = inShape[1]/2;   // Left Midpoint
            } else {
              int offset = 0;
              int nx = inShape[0];
              int ny = inShape[1];
              for (int i = 0; i < nx - 1; i++) {
                inEdgePoints(offset+i)[0] = i;
              }
              offset += nx;
              for (int i = 0; i < ny - 1; i++) {
                inEdgePoints(offset+i)[0] = nx - 1;
                inEdgePoints(offset+i)[1] = i;
              }
              offset += ny;
              for (int i = 0; i < nx - 1; i++) {
                inEdgePoints(offset+i)[0] = nx - 1 - i;
                inEdgePoints(offset+i)[1] = ny - 1;
              }
              offset += nx;
              for (int i = 0; i < ny - 1; i++) {
                inEdgePoints(offset+i)[0] = 0;
                inEdgePoints(offset+i)[1] = ny - 1 - i;
              }
            }

            // convert coordinates

            Vector<Double> pix(2);
            MDirection tempDir;
            if (x) {
                x->resize(nPoints);
            }
            if (y) {
                y->resize(nPoints);
            }

            for (int i = 0; i < nPoints; i++) {
              pix[0] = Double(inEdgePoints(i)[0]);
              pix[1] = Double(inEdgePoints(i)[1]);
              Bool success = inDC.toWorld(tempDir, pix);
              ASKAPCHECK(success,
                  "Pixel to world coordinate conversion failed for input "<< inEdgePoints(i)
                  <<" : "<< inDC.errorMessage());
              success = refDC.toPixel(pix,tempDir);
              ASKAPCHECK(success,
                  "World to pixel coordinate conversion failed for output "<< tempDir
                  << " : "<< refDC.errorMessage());
              outEdgePoints(i)[0] = casacore::Int(round(pix[0]));
              outEdgePoints(i)[1] = casacore::Int(round(pix[1]));
              if (x) {
                  (*x)(i) = pix[0];
              }
              if (y) {
                  (*y)(i) = pix[1];
              }
            }
            return outEdgePoints;
        }

        template<typename T>
        bool LinmosAccumulator<T>::coordinatesAreConsistent(const CoordinateSystem& coordSys1,
                                                            const CoordinateSystem& coordSys2) {
            // Check to see if it makes sense to combine images with these coordinate systems.
            // Could get more tricky, but right now make sure any extra dimensions, such as frequency
            // and polarisation, are equal in the two systems.
            if ( coordSys1.nCoordinates() != coordSys2.nCoordinates() ) {
                ASKAPLOG_INFO_STR(linmoslogger,
                    "Coordinates are not consistent: dimension mismatch");
                return false;
            }
            if (!allEQ(coordSys1.worldAxisNames(), coordSys2.worldAxisNames())) {
                ASKAPLOG_INFO_STR(linmoslogger,
                    "Coordinates are not consistent: axis name mismatch");

                for (casacore::uInt dim=0; dim<coordSys1.nCoordinates(); ++dim) {
                    ASKAPLOG_INFO_STR(linmoslogger,
                      "Axis " << dim << ":" << coordSys1.worldAxisNames()[dim] << " == " << coordSys2.worldAxisNames()[dim] );
                }

                return false;
            }
            if (!allEQ(coordSys1.worldAxisUnits(), coordSys2.worldAxisUnits())) {
                ASKAPLOG_INFO_STR(linmoslogger,
                    "Coordinates are not consistent: axis unit mismatch");
                return false;
            }
            return true;
        }

        // Check that the input coordinate system is the same as the output
        template<typename T>
        bool LinmosAccumulator<T>::coordinatesAreEqual(void) const {

            return coordinatesAreEqual(itsInCoordSys, itsOutCoordSys,
                                       itsInShape, itsOutShape);
        }

        // Check that two coordinate systems are the same
        template<typename T>
        bool LinmosAccumulator<T>::coordinatesAreEqual(const CoordinateSystem& coordSys1,
                                                       const CoordinateSystem& coordSys2,
                                                       const IPosition& shape1,
                                                       const IPosition& shape2) {

            // Set threshold for allowed small numerical differences
            double thresh = 1.0e-12;
            // Check that the shape is the same.
            if ( shape1 != shape2 ) {
                ASKAPLOG_DEBUG_STR(linmoslogger,
                    "Coordinates not equal: shape mismatch");
                return false;
            }
            else {
              ASKAPLOG_DEBUG_STR(linmoslogger,
                 "Coordinates are equal: " << shape1 << " == " << shape2);
            }
            // Check that the systems have consistent axes.
            if ( !coordinatesAreConsistent(coordSys1, coordSys2) ) {
              ASKAPLOG_DEBUG_STR(linmoslogger,
                 "Coordinates are not consistent");
                return false;
            }
            else {
              ASKAPLOG_DEBUG_STR(linmoslogger,
                 "Coordinates are consistent");
            }
            // check projections are the same
            if (coordSys1.hasDirectionCoordinate() && coordSys2.hasDirectionCoordinate() &&
                coordSys1.directionCoordinate().projection().type() !=
                coordSys2.directionCoordinate().projection().type()) {
                  ASKAPLOG_DEBUG_STR(linmoslogger,"Projections are not consistent");
                return false;
            }

            // test that the axes are equal
            ASKAPLOG_DEBUG_STR(linmoslogger,
              "nCoordinates: " << coordSys1.nCoordinates());
            for (casacore::uInt dim=0; dim<coordSys1.nCoordinates(); ++dim) {
                ASKAPLOG_DEBUG_STR(linmoslogger,
                   "Reference pixels are : " << coordSys1.referencePixel()[dim] << " == " << coordSys2.referencePixel()[dim] );

                ASKAPLOG_DEBUG_STR(linmoslogger,
                    "Reference values are : " << coordSys1.referenceValue()[dim] << " == " << coordSys2.referenceValue()[dim] );

                if ( (coordSys1.referencePixel()[dim] != coordSys2.referencePixel()[dim]) ||
                     (fabs(coordSys1.referenceValue()[dim] - coordSys2.referenceValue()[dim]) > thresh) ||
                     (fabs(coordSys1.increment()[dim] - coordSys2.increment()[dim]) > thresh) ) {
                      ASKAPLOG_DEBUG_STR(linmoslogger,
                         "Coordinates not equal: mismatch for dim " << dim);
                    return false;
                }
            }
            return true;
        }
        // set a default PB if required
        template<typename T>
        bool LinmosAccumulator<T>::setDefaultPB() {
          itsPBs.push_back(GaussianPB::createDefaultPrimaryBeam());
          return true;
        }

        /// @brief return the reference input image for a given output image
        template<typename T>
        std::string LinmosAccumulator<T>::getReference(const std::string& name) const {
            const std::map<std::string,std::vector<std::string> >::const_iterator ci = itsInImgNameVecs.find(name);
            ASKAPCHECK(ci != itsInImgNameVecs.end(), "Unable to find reference for the output image "<<name);
            const casacore::Int index = max(0,itsOutputRef);
            ASKAPCHECK(ci->second.size() > static_cast<size_t>(index), "Index "<<index<<" not found for "<<name);
            return ci->second[index];
        }

        /// @brief find beam number from image name, defaults to 0
        /// @param[in] name, the image name containing a beam specification
        /// @return int: the beam number found, or 0
        /// @details assumes image names look something like this:
        ///   image.i.SB10609.cont.Hydra_1A.beam35.taylor.1.restored.fits
        /// This example would return 35
        template<typename T>
        int LinmosAccumulator<T>::getBeamFromImageName(const string &name) {
            int beamNumber = 0;
            size_t pos0 = name.find(".beam");
            size_t pos1 = name.find(".",pos0+1);
            if (pos1==string::npos) {
                pos1 = name.size();
            }
            if (pos0!=string::npos && pos1 > pos0+5) {
                const string num = string(name,pos0+5,pos1);
                stringstream ss(num);
                try {
                    ss >> beamNumber;
                } catch (std::ios_base::failure) {
                    beamNumber = 0;
                }
                if (beamNumber < 0) beamNumber = 0;
            }
            return beamNumber;
        }

        /// @brief calculates the min and max of the x and y indexes of the area of
        ///        the weights plane above the weights cutoff value.
        /// @param[out] xmin - minimum x value
        /// @param[out] xmax - maximum x value
        /// @param[out] ymin - minimum y value
        /// @param[out] xmax - maximum y value
        template<typename T>
        void LinmosAccumulator<T>::calcWeightInputShape( int& xmin, int& xmax,
                                                        int& ymin, int& ymax)
        {
            const casacore::IPosition shape = itsInWgtBuffer.shape();
            const int nx = shape(0);
            const int ny = shape(1);

            // sort of global variables to store min and max values from
            // openmp threads
            int xMinGlobal = nx;
            int xMaxGlobal = 0;
            int yMinGlobal = ny;
            int yMaxGlobal = 0;

            T minVal, maxVal;
            IPosition minPos, maxPos;
            minMax(minVal,maxVal,minPos,maxPos,itsInWgtBuffer);
            ASKAPLOG_INFO_STR(linmoslogger, "Primary beam weighting - maxVal: " << maxVal );
            T wgtCutoff = itsCutoff * itsCutoff * maxVal; // itsOutWgtBuffer is prop. to image (gain/sigma)^2
            #pragma omp parallel
            {
                // variables local to each openmp thread
                int xMinLocal = nx;
                int xMaxLocal = 0;
                int yMinLocal = ny;
                int yMaxLocal = 0;
                #pragma omp for
                for(int y=0; y<ny; ++y) {
                    for (int x=0; x<nx; ++x) {
                        T val = itsInWgtBuffer.getAt(casacore::IPosition(2,x,y));
                        if (val > wgtCutoff) {
                            if ( x < xMinLocal ) xMinLocal = x;
                            if ( x > xMaxLocal ) xMaxLocal = x;
                            if ( y < yMinLocal ) yMinLocal = y;
                            if ( y > yMaxLocal ) yMaxLocal = y;
                        }
                    }
                }
                #pragma omp critical
                {
                    if (xMinGlobal > xMinLocal) xMinGlobal = xMinLocal;
                    if (xMaxGlobal < xMaxLocal) xMaxGlobal = xMaxLocal;
                    if (yMinGlobal > yMinLocal) yMinGlobal = yMinLocal;
                    if (yMaxGlobal < yMaxLocal) yMaxGlobal = yMaxLocal;
                } // critical
            } // parallel

            xmin = xMinGlobal;
            xmax = xMaxGlobal;
            ymin = yMinGlobal;
            ymax = yMaxGlobal;
        }
    } // namespace imagemath
} // namespace askap
