

#include <string>
#include <casacore/casa/Quanta/Quantum.h>
#include <casacore/casa/Quanta/MVDirection.h>

#include <utils/ImagemathUtils.h>

casacore::MVDirection convertDir(const std::string &ra, const std::string &dec) {
    casacore::Quantity tmpra,tmpdec;
    casacore::Quantity::read(tmpra, ra);
    // following code copied from AskapUtil::convertLatitude
    // we should probably move this routine there
    // Converts a colon separated latitude coordinate,
    // a format not supported by casacore, to a format that is
    // supported.
    std::string out(dec);
    size_t i = out.find_first_of(":");
    if (i != string::npos) {
        out[i] = 'd';
        i = out.find_first_of(":");
        if (i != string::npos) {
            out[i] = 'm';
        }
    }

    casacore::Quantity::read(tmpdec,out);
    return casacore::MVDirection(tmpra,tmpdec);
}
