cmake_minimum_required (VERSION 3.12.0)

include_guard()

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(VERSION "1.15.1")
project(base-imagemath VERSION ${VERSION} LANGUAGES C CXX)

find_package(Git REQUIRED)

include(CheckCXXCompilerFlag)
include (GNUInstallDirs)
# Load the version IDs of all the dependency sub repos
include(dependencies.cmake)

message (STATUS "Forming build label")
execute_process(
   COMMAND git describe --tags --always --dirty
   OUTPUT_VARIABLE BUILD_STR
   OUTPUT_STRIP_TRAILING_WHITESPACE
)
message(STATUS "Build label is ${BUILD_STR}")

message (STATUS "DEV_OVERRIDES = $ENV{DEV_OVERRIDES}")
if(DEFINED ENV{DEV_OVERRIDES})
  message (STATUS "cmake config overrides file specified, $ENV{DEV_OVERRIDES}")
  include($ENV{DEV_OVERRIDES} OPTIONAL)
else()
  message (STATUS "No cmake config overrides file specified, using defaults")
endif()

message(STATUS "CMAKE_INSTALL_PREFIX is ${CMAKE_INSTALL_PREFIX}")

# Declare all of sub repos and tools
include(FetchContent)
# Put our askap cmake modules in the cmake path
# We need this before any other Fetch calls so we have the find_package routines available
# -------- askap cmake modules --------
FetchContent_Declare(askap-cmake
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/askap-cmake.git
  GIT_TAG           ${ASKAP_CMAKE_TAG}
  SOURCE_DIR        askap-cmake
)

message (STATUS "Fetching askap-cmake files")
FetchContent_GetProperties(askap-cmake)
if(NOT askap-cmake_POPULATED)
  FetchContent_Populate(askap-cmake)
endif()

list(APPEND CMAKE_MODULE_PATH "${askap-cmake_SOURCE_DIR}")
#message(STATUS "askap-cmake path is ${askap-cmake_SOURCE_DIR}")
message(STATUS "askap-cmake path is ${CMAKE_MODULE_PATH}")

include(version_utils)
get_version_string()

# -------- lofar common --------
FetchContent_Declare(lofar-common
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/lofar-common.git
  GIT_TAG           ${LOFAR_COMMON_TAG}
  SOURCE_DIR        lofar-common
)

# -------- lofar blob --------
FetchContent_Declare(lofar-blob
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/lofar-blob.git
  GIT_TAG           ${LOFAR_BLOB_TAG}
  SOURCE_DIR        lofar-blob
)

# -------- base-askap --------
FetchContent_Declare(base-askap
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-askap.git
  GIT_TAG           ${BASE_ASKAP_TAG}
  SOURCE_DIR        base-askap
)

message (STATUS "Fetching sub repos")
FetchContent_MakeAvailable( lofar-common lofar-blob base-askap)
message (STATUS "Done - Fetching sub repos")

option (CXX11 "Compile as C++11 if possible" NO)
option (ENABLE_SHARED "Build shared libraries" YES)
option (ENABLE_RPATH "Include rpath in executables and shared libraries" YES)

# Required Version of ASKAP dependencies is the MAJOR and MINOR version
# of this package. This allows the PATCH versions to change on everything
# but the version number cannot be less or more than the current version
#
set(REQUIRED_VERSION "${VERSION_MAJOR}.${VERSION_MINOR}")

configure_file(askap_imagemath.cc.in askap_imagemath.cc)

# uninstall target
if(NOT TARGET uninstall)
    configure_file(
        "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
        "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
        IMMEDIATE @ONLY)

    add_custom_target(uninstall
        COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
endif()


#
# HACK HACK HACK
#
# These are needed because the filesystem structure of our sources
# doesn't follow the include paths that users require.
# Once that is fixed (i.e., once the top-level primarybeam, linmos
# and utils directories are moved into askap/imagemath) this will
# not be necessary anymore.

execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory
  askap/imagemath
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
execute_process(COMMAND ${CMAKE_COMMAND} -E create_symlink
  ${CMAKE_CURRENT_SOURCE_DIR}/primarybeam
  ${CMAKE_CURRENT_BINARY_DIR}/askap/imagemath/primarybeam)
execute_process(COMMAND ${CMAKE_COMMAND} -E create_symlink
  ${CMAKE_CURRENT_SOURCE_DIR}/linmos
  ${CMAKE_CURRENT_BINARY_DIR}/askap/imagemath/linmos)
execute_process(COMMAND ${CMAKE_COMMAND} -E create_symlink
  ${CMAKE_CURRENT_SOURCE_DIR}/utils
  ${CMAKE_CURRENT_BINARY_DIR}/askap/imagemath/utils)

#
# HACK HACK HACK
#


if (ENABLE_SHARED)
option (BUILD_SHARED_LIBS "" YES)
    if (ENABLE_RPATH)
        # Set RPATH to use for installed targets; append linker search path
        set(CMAKE_INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib" )
        set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
        set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
    endif (ENABLE_RPATH)
endif(ENABLE_SHARED)

# find packages
#find_package(lofar-common REQUIRED)
#find_package(askap-askap REQUIRED)
find_package(log4cxx REQUIRED)
find_package(Casacore REQUIRED COMPONENTS  ms images mirlib coordinates fits lattices measures scimath scimath_f tables casa)
find_package(CPPUnit)

if (CXX11)
    message(STATUS "---> CXX11 is set")
	set(CMAKE_CXX_STANDARD 11)
	set(CMAKE_CXX_STANDARD_REQUIRED ON)
endif()

add_library(imagemath

${CMAKE_CURRENT_BINARY_DIR}/askap_imagemath.cc
primarybeam/PrimaryBeamFactory.cc
primarybeam/PrimaryBeam.cc
primarybeam/GaussianPB.cc
primarybeam/MWA_PB.cc
primarybeam/SKA_LOW_PB.cc
primarybeam/ASKAP_PB.cc
linmos/Interpolate2D.cc
utils/ImagemathUtils.cc
utils/MultiDimArrayPlaneIter.cc
)

set_target_properties(imagemath PROPERTIES OUTPUT_NAME askap_imagemath)

install (FILES

linmos/LinmosAccumulator.h
linmos/LinmosAccumulator.tcc
linmos/LinmosImageRegrid.h
linmos/LinmosImageRegrid.tcc
linmos/Interpolate2D.h
linmos/Interpolate2D2.tcc
DESTINATION include/askap/imagemath/linmos

)

install (FILES

utils/ImagemathUtils.h
utils/MultiDimArrayPlaneIter.h
utils/MultiDimArrayPlaneIter.tcc

DESTINATION include/askap/imagemath/utils
)

install (FILES

primarybeam/GaussianPB.h
primarybeam/MWA_PB.h
primarybeam/SKA_LOW_PB.h
primarybeam/ASKAP_PB.h
primarybeam/PrimaryBeam.h
primarybeam/PrimaryBeamFactory.h
DESTINATION include/askap/imagemath/primarybeam
)



target_link_libraries(imagemath PUBLIC
	lofar::Common
	askap::askap
	${CASACORE_LIBRARIES}
	${log4cxx_LIBRARY}
	${Boost_LIBRARIES}
)

add_library(askap::imagemath ALIAS imagemath)

target_include_directories(imagemath PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
  $<INSTALL_INTERFACE:include>
  $<INSTALL_INTERFACE:include/askap/imagemath>
  ${Boost_INCLUDE_DIRS}
  ${log4cxx_INCLUDE_DIRS}
  ${CASACORE_INCLUDE_DIRS}
)

target_compile_definitions(imagemath PUBLIC
  HAVE_AIPSPP
  HAVE_BOOST
  HAVE_LOG4CXX
)

install (
TARGETS imagemath
EXPORT askap-imagemath-targets
RUNTIME DESTINATION bin
LIBRARY DESTINATION lib
ARCHIVE DESTINATION lib
LIBRARY PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
)

include(CTest)
if (BUILD_TESTING)
  if (NOT CPPUNIT_FOUND)
    message(FATAL_ERROR "CPPUnit is needed for building and running unit tests")
  endif()
  enable_testing()
  include_directories(${CMAKE_CURRENT_SOURCE_DIR}/tests/primarybeam)
  include_directories(${CMAKE_CURRENT_SOURCE_DIR}/tests/utils)
  add_subdirectory(tests/primarybeam)
  add_subdirectory(tests/utils)
endif()


#include(yanda_export)
#yanda_export(askap)
